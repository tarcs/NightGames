package status;

import combat.Combat;

import characters.Anatomy;
import characters.Attribute;
import characters.Character;
import characters.Emotion;
import characters.Trait;

public class Sore extends Status {
	private Anatomy part;
	private float value;

	public Sore(Character affected, int duration, Anatomy part, float value) {
		super("Sore: "+part,affected);
		this.duration=duration;
		this.part = part;
		this.value = value;
		lingering = true;
		this.flag(Stsflag.sore);
		tooltip = "Increased pain to "+part;
		if(value<1.0f){
			this.name = "Numb: "+part;
			tooltip = "Decreased pain to "+part;
		}
		if(affected!=null && affected.has(Trait.PersonalInertia)){
			this.duration=3*duration/2;
		}
		this.affected = affected;
		
	}

	@Override
	public String describe() {
		return "";
	}
	@Override
	public int regen() {
		return -1;		
	}

	@Override
	public int value() {
		return (int) value;
	}
	public float sore(Anatomy targeted){
		if(part==targeted){
			return value;
		}
		return 1.0f;
	}
	@Override
	public Status copy(Character target) {
		return new Sore(target,duration,part,value);
	}
	@Override
	public void turn(Combat c) {
		affected.emote(Emotion.nervous,10);
		decay();
	}
}
