package status;

import combat.Combat;

import characters.Attribute;
import characters.Character;
import characters.Emotion;
import characters.Trait;

public class WaterStance extends Status {

	public WaterStance(Character affected) {
		super("Water Form",affected);
		this.duration=10;
		flag(Stsflag.form);
		tooltip = "Evasion and Counter bonus, Power penalty";
		this.affected = affected;
		if(affected!=null && affected.has(Trait.PersonalInertia)){
			this.duration=15;
		}
		
	}

	@Override
	public String describe() {
		if(affected.human()){
			return "You're as smooth and responsive as flowing water.";
		}
		else{
			return affected.name()+" continues her flowing movements.";
		}
	}

	@Override
	public int mod(Attribute a) {
		if(Attribute.Power==a){
			return -Math.min(affected.get(Attribute.Ki),30)/2;
		}
		return 0;
	}

	@Override
	public int evade() {
		// TODO Auto-generated method stub
		return Math.min(affected.get(Attribute.Ki),30);
	}

	@Override
	public int counter() {
		return Math.min(affected.get(Attribute.Ki),30)/2;
	}

	@Override
	public Status copy(Character target) {
		return new WaterStance(target);
	}
	@Override
	public void turn(Combat c) {
		affected.emote(Emotion.confident,5);
		decay(c);
	}

}
