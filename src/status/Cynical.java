package status;

import java.util.HashSet;

import combat.Combat;

import characters.Attribute;
import characters.Character;
import characters.Trait;

public class Cynical extends Status {

	public Cynical(Character affected) {
		super("Cynical",affected);
		flag(Stsflag.cynical);
		tooltip = "Immune to mind-affecting statuses";
		this.affected = affected;
		if(affected.has(Trait.PersonalInertia)){
			duration = 5;
		}else{
			duration = 3;
		}
	}

	@Override
	public String describe() {
		if(affected.human()){
			return "You're feeling more cynical than usual and won't fall for any mind games.";
		}
		else{
			return affected.name()+" has a cynical edge in her eyes.";
		}
	}

	@Override
	public int tempted(int x) {
		return -x/4;
	}

	@Override
	public int gainmojo(int x) {
		return -x;
	}

	@Override
	public Status copy(Character target) {
		return new Cynical(target);
	}
}
