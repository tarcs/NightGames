package status;

import combat.Combat;

import characters.Attribute;
import characters.Character;
import characters.Trait;

public class BD extends Status{

	public BD(Character affected) {
		super("Bondage", affected);
		magnitude = 7;
		flag(Stsflag.bondage);
		if(affected!=null && affected.has(Trait.PersonalInertia)){
			duration = 15;
		}else{
			duration = 10;
		}
		this.affected = affected;
		tooltip = "Gain arousal each turn if bound";
	}

	@Override
	public String describe() {
		if(affected.human()){
			return "Fantasies of being tied up continue to dance through your head.";
		}
		else{
			return affected.name()+" is affected by a brief bondage fetish.";
		}
	}

	@Override
	public Status copy(Character target) {
		return new BD(target);
	}
	@Override
	public void turn(Combat c) {
		if(affected.bound() || affected.is(Stsflag.tied)){
			affected.tempt(magnitude);
		}
		decay(c);
	}

}
