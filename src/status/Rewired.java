package status;

import characters.Anatomy;
import combat.Combat;

import characters.Attribute;
import characters.Character;
import characters.Trait;

public class Rewired extends Status {

	public Rewired(Character affected, int duration) {
		super("Rewired",affected);
		this.duration=duration;
		lingering = true;
		flag(Stsflag.rewired);
		tooltip = "Pleasure reduces Stamina and Pain raises Arousal";
		if(affected!=null && affected.has(Trait.PersonalInertia)){
			this.duration=3*duration/2;
		}
		this.affected = affected;
		
	}

	@Override
	public String describe() {
		if(affected.human()){
			return "Your senses feel... wrong. It's like your sense of pleasure and pain are jumbled.";
		}
		else{
			return affected.name()+" fidgets uncertainly at the alien sensation of her rewired nerves.";
		}
	}

	@Override
	public int damage(int x, Anatomy area) {
		if(area!=Anatomy.soul) {
			affected.getArousal().restore(x);
		}
		return -x;
	}

	@Override
	public int pleasure(int x, Anatomy area) {
		if(area!=Anatomy.soul) {
			affected.getStamina().reduce(x);
		}
		return -x;
	}

	@Override
	public Status copy(Character target) {
		return new Rewired(target,duration);
	}

}
