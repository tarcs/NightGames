package status;

import combat.Combat;

import characters.Attribute;
import characters.Character;
import characters.Emotion;
import characters.Trait;

public class FireStance extends Status {

	public FireStance(Character affected) {
		super("Fire Form", affected);
		duration = 10;
		flag(Stsflag.form);
		tooltip = "Mojo skills cost half, increased Mojo gain, lose Stamina each turn";
		if(affected!=null && affected.has(Trait.PersonalInertia)){
			duration = 15;
		}
		this.affected = affected;
	}

	@Override
	public String describe() {
		if(affected.human()){
			return "Your spirit burns in you, feeding your power";
		}
		else{
			return affected.name()+" is still all fired up.";
		}
	}

	@Override
	public int regen() {
		return -Math.min(affected.get(Attribute.Ki),30)/5;
	}

	@Override
	public int gainmojo(int x) {
		return x*Math.min(affected.get(Attribute.Ki),30)/6;
	}

	@Override
	public int spendmojo(int x) {
		return -x/2;
	}

	@Override
	public Status copy(Character target) {
		return new FireStance(target);
	}
	@Override
	public void turn(Combat c) {
		affected.emote(Emotion.confident,5);
		affected.emote(Emotion.dominant,5);
		decay(c);
	}

}
