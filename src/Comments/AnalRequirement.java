package Comments;
import characters.Character;
import combat.Combat;
import stance.Stance;

public class AnalRequirement implements CustomRequirement {

	private final boolean anal;
	
	public AnalRequirement(boolean anal) {
		this.anal = anal;
	}

	@Override
	public boolean meets(Combat c, Character self, Character other) {
		if (c == null)
			return false;
		return (c.stance.en == Stance.anal && c.stance.penetration(other)) == anal;
	}

}