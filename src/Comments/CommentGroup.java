package Comments;

import java.util.ArrayList;
import java.util.HashMap;

public class CommentGroup {
	private HashMap<CommentSituation,ArrayList<String>> comments;
	
	public CommentGroup(){
		comments = new HashMap<CommentSituation,ArrayList<String>>();
		for(CommentSituation situation: CommentSituation.values()){
			comments.put(situation, new ArrayList<String>());
		}
	}
	
	public void put(CommentSituation situation, String comment){
		comments.get(situation).add(comment);
	}
	
	public ArrayList<String> getComments(CommentSituation situation){
		return comments.get(situation);
	}
}
