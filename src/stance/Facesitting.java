package stance;


import characters.Anatomy;
import characters.Attribute;
import characters.Character;
import characters.Trait;
import combat.Combat;
import combat.Result;
import global.Global;
import status.Enthralled;
import status.Horny;
import status.Shamed;

public class Facesitting extends Position {

    public Facesitting(Character top, Character bottom) {
        super(top, bottom,Stance.facesitting);
        strength = 50;
    }

    @Override
    public String describe() {
        if(top.human()){
            return "You are sitting on "+bottom.name()+"'s face, her mouth forced to pleasure your genitals while you have free access to her body.";
        }
        else{
            return top.name()+" is sitting on your face, forcing you to service her with your mouth while she enjoys free access to the rest of your body.";
        }
    }

    @Override
    public boolean mobile(Character c) {
        return c==top;
    }

    @Override
    public boolean kiss(Character c) {
        return false;
    }

    @Override
    public boolean dom(Character c) {
        return c==top;
    }

    @Override
    public boolean sub(Character c) {
        return c==bottom;
    }

    @Override
    public boolean reachTop(Character c) {
        return c==top;
    }

    @Override
    public boolean reachBottom(Character c) {
        return true;
    }

    @Override
    public boolean prone(Character c) {
        return c==bottom;
    }

    @Override
    public boolean feet(Character c) {
        return c==top;
    }

    @Override
    public boolean oral(Character c) {
        return true;
    }

    @Override
    public boolean behind(Character c) {
        return false;
    }

    @Override
    public boolean penetration(Character c) {
        return false;
    }

    @Override
    public Position insert(Character c) {
        return new ReverseMount(top,bottom);
    }

    public void checkOngoing(Combat c){
        int m = 2 + (2 * pace);
        int r = Math.max(1, 2-pace); // top recoil damage is harder to mitigate
        if(top.has(Trait.experienced)){
            r = r*2;
        }
        if(top.human()){
            if(pace>1){
                c.write(top,"Your intense facefucking continues to drive you both closer to ecstasy.");
            }else if(pace == 1){
                c.write(top,"Your shaft rests inside her mouth, and her submissive licking and kissing turns you both on.");
            }else{
                c.write(top,"You kneel with your balls in her mouth, while she submissively sucks on them.");
            }
        }else if(!top.human()&&top.hasDick()){
            if(pace>1){
                if(top.getArousal().percent()>=75){
                    c.write(top,"Her furious facefucking of your mouth continues to arouse both of you as she leaks precum from her tip.");
                }else{
                    c.write(top,"Her furious facefucking of your mouth continues to arouse both of you.");
                }
            }else if(pace == 1){
                c.write(top,"Her steady alternating grinding of her pussy and cock into your mouth continues to erode your resistance.");
            }else{
                c.write(top,"She slowly but powerfully grinds her pussy and balls into your mouth, dominating you intensely.");
            }
        }else{
            if(pace>1){
                c.write(top,"Her furious back-and-forth grinding on your face continues to arouse both of you.");
            }else if(pace == 1){
                c.write(top,"Her steady grinding into your face continues to erode your resistance.");
            }else{
                c.write(top,"She slowly but powerfully grinds her pussy into your mouth, dominating you intensely.");
            }
        }
        if (top.has(Trait.succubus)||top.has(Trait.lacedjuices)){
            if (Global.random(4) == 0) {
                if(top.get(Attribute.Dark)>=6&&Global.random(3)==0){
                    bottom.tempt(m+4,c);
                    bottom.add(new Enthralled(bottom,top),c);
                    if(top.human()){
                        c.write(top,"Your incubus penis enthralls "+bottom.name()+".");
                    }else{
                        c.write(top,top.name()+"'s juices are simply too much for you, and you feel your mind being enthralled by the pussy above you.");
                    }
                }
                else{
                    bottom.tempt(m+4,c);
                    if(top.human()){
                        c.write(top,"Your incubus penis is extra tempting to "+bottom.name());
                    }else{
                        c.write(top,top.name()+"'s aphrodisiac-laced juices flow into your mouth and you eagerly lap them up.");
                    }
                }
            }else{ // didn't get succubus crit
                bottom.tempt(m,c);
            }
        }else if (top.has(Trait.pheromones)){
            if(Global.random(6) == 0){
                bottom.tempt(m+1,c);
                bottom.add(new Horny(bottom,4,3),c);
                c.write(top,"Smelling "+top.name()+"'s pheromones close up awakens a primal need in you, though you'll have trouble fulfilling it from down here.");
            }
        }else{
            bottom.tempt(m,c);
        }
        bottom.add(new Shamed(bottom),c);
        bottom.modMojo(-3 - pace^2);
        top.buildMojo(3 + 3*pace^2);
        top.pleasure(bottom.bonusProficiency(Anatomy.mouth, m/r), Anatomy.genitals, c);
    }

}
