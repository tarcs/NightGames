package skills;

import characters.*;
import characters.Character;
import combat.Combat;
import combat.Result;
import global.Flag;
import global.Global;
import global.Modifier;
import global.Scheduler;
import items.Attachment;
import items.Toy;
import status.Stsflag;

public class WarningCrop extends Skill {

	public WarningCrop(Character self) {
		super("Warning Crop", self);
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Discipline)>=3;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return (self.has(Toy.Crop)||self.has(Toy.Crop2)||self.has(Toy.Crop3))&&self.canAct()&&c.stance.mobile(self)&&(c.stance.reachTop(self)||c.stance.reachBottom(self))
				&&(!self.human()|| Scheduler.getMatch().condition!=Modifier.notoys);
	}

	@Override
	public void resolve(Combat c, Character target) {
		int m;
		m = 2+Global.random(8)+(self.get(Attribute.Discipline)/3);
		m = self.bonusProficiency(Anatomy.toy, m);
		if(self.human()){
			c.write(self,deal(0,Result.normal,target));
		}
		else if(target.human()){
			c.write(self,receive(0,Result.normal,target));
		}
		target.pain(m,Anatomy.fingers,c);
		target.spendMojo(2*m);
		target.emote(Emotion.nervous,70);
	}

	@Override
	public Skill copy(Character user) {
		return new WarningCrop(user);
	}

	@Override
	public Tactics type() {
		return Tactics.status;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		return "Just as it looks like "+target.name()+" is thinking of striking you, you lash out with your riding crop. The tip of it strikes across the back of her hand and she immediately pulls it back. That should make her think twice about that.";
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		return "Just as your mind flashes to the possibility of striking at "+self.name()+", she lashes out with her riding crop, striking across the back of your hand. You pull your hand back on instinct. "+self.pronounSubject(true)+" must have seen through you and knew what you were thinking. You’ll have to be more careful in the future.";
	}

	@Override
	public String describe() {
		return "Use a light crop strike to demoralize your opponent";
	}

}
