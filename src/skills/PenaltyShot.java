package skills;

import characters.Anatomy;
import characters.Attribute;
import characters.Character;
import characters.Trait;
import combat.Combat;
import combat.Result;
import global.Global;
import stance.Mount;
import stance.Stance;

public class PenaltyShot extends Skill {
    public PenaltyShot(Character self) {
        super("Penalty Shot", self);
    }

    @Override
    public boolean requirements(Character user) {
        return user.getPure(Attribute.Footballer)>=12;
    }

    @Override
    public boolean usable(Combat c, Character target) {
        return (c.stance.en == Stance.mount || c.stance.en == Stance.pin)
                &&c.stance.sub(self)
                &&self.canAct()
                &&target.canAct()
                &&!c.stance.penetration(self)
                &&!c.stance.penetration(target)
                &&self.canSpend(10);
    }

    @Override
    public String describe() {
        return "Kiss your opponent to distract them, then surprise them with a knee strike: 10 Mojo";
    }

    @Override
    public void resolve(Combat c, Character target) {
        self.spendMojo(10);
        target.tempt(Global.random(4)+self.get(Attribute.Seduction)/4,c);
        if(c.attackRoll(this,self,target)){
            if (self.human()) {
                c.write(self, deal(0, Result.normal, target));
            } else if (target.human()) {
                c.write(self, receive(0, Result.normal, target));
            }
            target.pain(4+Global.random(11)+self.get(Attribute.Footballer), Anatomy.genitals,c);
            if(c.stance.en == Stance.pin){
                c.stance = new Mount(target,self);
            }
        }else{
            if (self.human()) {
                c.write(self, deal(0, Result.miss, target));
            } else if (target.human()) {
                c.write(self, receive(0, Result.miss, target));
            }
        }
        if(c.stance.en == Stance.pin){
            c.stance = new Mount(target,self);
        }

    }

    @Override
    public int speed(){
        return 2;
    }

    @Override
    public int accuracy() {
        return 7;
    }
    @Override

    public Skill copy(Character user) {
        return new PenaltyShot(user);
    }

    @Override
    public Tactics type() {
        return Tactics.damage;
    }

    @Override
    public String deal(int damage, Result modifier, Character target) {
        if(modifier == Result.miss){
            return "You pull "+target.name()+" down into a passionate kiss.  Your tongue tangles with hers, but when you try to move your legs, she's not as " +
                    "distracted as you hoped. She braces her body and holds you in place.";
        }
        return "You pull "+target.name()+" down into a passionate kiss.  Your skilled tongue completely captures her attention, and she seems too distracted to " +
                "notice that you are slowly parting her thighs with your knees.  You catch her by surprise and drive your knee upwards into her groin.";
    }

    @Override
    public String receive(int damage, Result modifier, Character target) {
        if(modifier == Result.miss){
            return "With an alluring twinkle in her eye, "+self.name()+" grabs your shoulders and pulls you down into a passionate kiss.  " +
                    "Your mind almost goes blank, but you still feel her try to push your legs open. You quickly clamp your thighs together to stop her.";
        }
        return "With an alluring twinkle in her eye, "+self.name()+" grabs your shoulders and pulls you down into a passionate kiss.  " +
                "Your dick is rock hard and throbbing, and you see this as an excellent opportunity to reposition your legs to penetrate her.  " +
                "The moment you open your thighs, however, she pumps her knee straight upwards between your legs, smashing your balls as well as your erection.";
    }
}
