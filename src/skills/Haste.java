package skills;

import characters.Attribute;
import characters.Character;
import combat.Combat;
import combat.Result;
import status.Abuff;
import status.Primed;

public class Haste extends Skill {

	public Haste(Character self) {
		super("Haste", self);
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Temporal)>=1;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return c.stance.mobile(self)&&!c.stance.prone(self)&&self.canAct()&&self.getStatusMagnitude("Primed")>=1&&self.getStatusMagnitude("Speed buff")<10;
	}

	@Override
	public String describe() {
		return "Temporarily buffs your speed: 1 charge";
	}

	@Override
	public void resolve(Combat c, Character target) {
		self.add(new Primed(self,-1));
		
		if(self.human()){
			c.write(self,deal(0,Result.normal,target));
		}
		else if(target.human()){
			c.write(self,receive(0,Result.normal,target));
		}
		self.add(new Abuff(self,Attribute.Speed,10,6),c);
	}

	@Override
	public Skill copy(Character user) {
		return new Haste(user);
	}

	@Override
	public Tactics type() {
		return Tactics.preparation;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		return String.format("You spend a stored time charge. The world around you appears to slow down as your personal time accelerates." );
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		return String.format("%s hits a button on %s wristwatch and suddenly speeds up. %s is moving so fast that %s seems to blur.",
				self.name(),self.possessive(false),self.pronounSubject(true),self.pronounSubject(false) );
	}

}
