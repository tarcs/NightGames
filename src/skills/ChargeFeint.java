package skills;

import characters.Attribute;
import characters.Character;
import combat.Combat;
import combat.Result;
import status.Flatfooted;

public class ChargeFeint extends Skill {
    public ChargeFeint(Character self) {
        super("Charge Feint", self);
    }

    @Override
    public boolean requirements(Character user) {
        return user.getPure(Attribute.Footballer) >= 1;
    }

    @Override
    public boolean usable(Combat c, Character target) {
        return self.canActNormally(c);
    }

    @Override
    public String describe() {
        return "Fake out your opponent with fancy footwork";
    }

    @Override
    public void resolve(Combat c, Character target) {
        if(c.effectRoll(this,self,target,self.get(Attribute.Footballer))){
            if (self.human()) {
                c.write(self,deal(0,Result.normal,target));
            } else {
                c.write(self,receive(0,Result.normal,target));
            }
            int duration = 1;
            if(self.getPure(Attribute.Footballer)>=12){
                duration ++;
            }
            target.add(new Flatfooted(target,duration),c);
        }else{
            if (self.human()) {
                c.write(self,deal(0,Result.miss,target));
            } else {
                c.write(self,receive(0,Result.miss,target));
            }
        }
    }

    @Override
    public Skill copy(Character user) {
        return new ChargeFeint(user);
    }

    @Override
    public Tactics type() {
        return Tactics.status;
    }

    @Override
    public String deal(int damage, Result modifier, Character target) {
        if(modifier==Result.miss){
            return "";
        }
        else{
            return "";
        }
    }

    @Override
    public String receive(int damage, Result modifier, Character target) {
        if(modifier==Result.miss){
            return "";
        }
        else{
            return "";
        }
    }
}
