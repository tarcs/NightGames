package skills;

import characters.Attribute;
import characters.Character;
import combat.Combat;
import combat.Result;
import status.Composed;
import status.Stsflag;

public class RegainComposure extends Skill {
    public RegainComposure(Character self) {
        super("Regain Composure", self);
    }

    @Override
    public boolean requirements(Character user) {
        return user.getPure(Attribute.Discipline)>=21;
    }

    @Override
    public boolean usable(Combat c, Character target) {
        return self.canActNormally(c)&&self.is(Stsflag.broken)&&!target.canAct()&&self.canSpend(40);
    }

    @Override
    public String describe() {
        return "Take a moment to restore your composed state: 40 Mojo";
    }

    @Override
    public void resolve(Combat c, Character target) {
        self.spendMojo(40);
        if(self.human()){
            c.write(self,deal(0,Result.normal,target));
        }
        else if(target.human()){
            c.write(self,receive(0,Result.normal,target));
        }
        self.removeStatus(Stsflag.broken,c);
        self.add(new Composed(self,(5+(self.get(Attribute.Discipline)/2))/2,self.get(Attribute.Discipline)),c);
    }

    @Override
    public Skill copy(Character user) {
        return new RegainComposure(user);
    }

    @Override
    public Tactics type() {
        return Tactics.calming;
    }

    @Override
    public String deal(int damage, Result modifier, Character target) {
        return "While it's tempting to take advantage of "+target.name()+" in her moment of vulnerability, you decide that your time right now would be better spend trying to get ahold of yourself. You take a few deep breaths, trying to calm yourself from the heat of battle and focus your thoughts once more. With your emotions under control again, you turn your mind back to the match.";
    }

    @Override
    public String receive(int damage, Result modifier, Character target) {
        return "As you struggle to break free, "+self.name()+" seems to stop paying attention to you for a few seconds. She closes her eyes and breathes deeply, and her body noticeably relaxes. When her eyes reopen, she's regained her prior focus.";
    }
}
