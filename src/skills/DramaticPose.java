package skills;

import characters.Attribute;
import characters.Character;
import characters.Emotion;
import combat.Combat;
import combat.Result;

public class DramaticPose extends Skill{
    public DramaticPose(Character self){
        super("Dramatic Pose",self);
    }

    @Override
    public boolean requirements(Character user) {
        return user.getPure(Attribute.Contender)>=1;
    }

    @Override
    public boolean usable(Combat c, Character target) {
        return self.canActNormally(c);
    }

    @Override
    public String describe() {
        return "Gain mojo when close to being stunned or cumming.";
    }

    @Override
    public void resolve(Combat c, Character target) {
        int x = 0;
        x += (100-self.getStamina().percent())/2;
        x += self.getArousal().percent()/2;
        if(self.human()){
            c.write(self,deal(0,Result.normal,target));
        }
        else if(target.human()){
            c.write(self,receive(0,Result.normal,target));
        }
        self.emote(Emotion.confident,100);
        self.buildMojo(x);
    }

    @Override
    public Skill copy(Character user) {
        return new DramaticPose(user);
    }

    @Override
    public Tactics type() {
        return Tactics.recovery;
    }

    @Override
    public String deal(int damage, Result modifier, Character target) {
        return "Even though the fight isn't going well, you strike a confident pose. Your counterattack starts now.";
    }

    @Override
    public String receive(int damage, Result modifier, Character target) {
        return "Despite being worn down, "+self.name()+" poses dramatically.";
    }
}
