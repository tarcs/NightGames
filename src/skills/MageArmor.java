package skills;

import items.Clothing;
import characters.Attribute;
import characters.Character;

import combat.Combat;
import combat.Result;
import global.Global;

public class MageArmor extends Skill {

	public MageArmor(Character self) {
		super("Mage Armor", self);
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Arcane)>=21;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct()&&c.stance.mobile(self)&&!c.stance.sub(self)&&self.pantsless()&&self.canSpend(20)&&!c.stance.penetration(self)&&!c.stance.penetration(target);
	}

	@Override
	public String describe() {
		return "Craft some replacement underwear out of pure magic: 20 Mojo";
	}

	@Override
	public void resolve(Combat c, Character target) {
		if(self.canWear(Clothing.magethong)){
			self.wear(Clothing.magethong);
		}
		if(self.human()){
			c.write(self,deal(0,Result.normal,target));
		}else if(target.human()){
			c.write(self,receive(0,Result.normal,target));
		}
		self.spendMojo(20);
		
	}

	@Override
	public Skill copy(Character user) {
		return new MageArmor(user);
	}

	@Override
	public Tactics type() {
		return Tactics.calming;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		switch(Global.random(2)){
		case 1:
			 return "You conjure a meager thong to cover your genitals. It's not much, but it will have to do for now.";
		default:
			return "You channel your magic and evoke a glowing magic thong. It doesn't cover much, but it provides some basic protection.";
		}
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		switch(Global.random(2)){
		case 1:
			return self.name() + " channels her magic for a moment to magically make a magic thong appear on her crotch. Magically.";
		default:
			return self.name()+" goes into what appears to be a brief Magical Girl transformation sequence and a thong magically appears on her.";
		}
	}

}
