package skills;

import characters.Anatomy;
import characters.Attribute;
import characters.Character;
import combat.Combat;
import combat.Result;
import status.Horny;
import status.Sore;

public class SpreadThighs extends Skill{
    public SpreadThighs(Character self) {
        super("Spread Thighs", self);
    }

    @Override
    public boolean requirements(Character user) {
        return user.getPure(Attribute.Footballer) >= 6;
    }

    @Override
    public boolean usable(Combat c, Character target) {
        return self.pantsless() && self.canActNormally(c);
    }

    @Override
    public String describe() {
        return "Spread your legs to tempt your opponent. Can cause Horniness, but makes you more vulnerable to low blows.";
    }

    @Override
    public void resolve(Combat c, Character target) {
        self.add(new Sore(self, 2, Anatomy.genitals,2f));
        target.tempt(self.get(Attribute.Footballer)/2);
        if(c.effectRoll(this,self,target,self.get(Attribute.Footballer))){
            if (self.human()) {
                c.write(self,deal(0,Result.normal,target));
            } else {
                c.write(self,receive(0,Result.normal,target));
            }
            int duration = 1;
            if(self.getPure(Attribute.Footballer)>=12){
                duration ++;
            }
            target.add(new Horny(target,self.get(Attribute.Footballer)/2,duration),c);
        }else{
            if (self.human()) {
                c.write(self,deal(0,Result.miss,target));
            } else {
                c.write(self,receive(0,Result.miss,target));
            }
        }
    }

    public int speed(){
        return 9;
    }

    @Override
    public Skill copy(Character user) {
        return new SpreadThighs(user);
    }

    @Override
    public Tactics type() {
        return Tactics.status;
    }

    @Override
    public String deal(int damage, Result modifier, Character target) {
        if(modifier==Result.miss){
            return "You spread your legs and flaunt your package with as much flair as you can manage. "+target.name()+" grins approvingly at the sight, " +
                    "but doesn't seem to be overcome with lust.";
        }else{
            return "You spread your legs and flaunt your package with as much flair as you can manage. "+target.name()+" flushes and her eyes stay glued " +
                    "to your wagging member.";
        }
    }

    @Override
    public String receive(int damage, Result modifier, Character target) {
        if(modifier==Result.miss){
            return self.name()+" spreads her thighs with a seductive smile. She runs her hands down her belly and " +
                    "gives her crotch a sensual rub. You do your best to ignore the temptation. If you win, you'll " +
                    "claim that prize soon anyway.";
        }else{
            return self.name()+" spreads her thighs with a seductive smile. She runs her hands down her belly and " +
                    "gives her crotch a sensual rub. You catch yourself drooling at the sight. Your straining cock " +
                    "insists that you fill her as soon as possible.";
        }
    }
}
