package skills;

import characters.Attribute;
import characters.Character;

import combat.Combat;
import combat.Result;

public class FlyCatcher extends Skill {

	public FlyCatcher(Character self) {
		super("Fly Catcher", self);
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Ki)>=9;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return target.pet!=null&&self.canAct()&&c.stance.mobile(self)&&!c.stance.prone(self);
	}

	@Override
	public String describe() {
		return "Focus on eliminating the enemy pet: 5 Stamina";
	}

	@Override
	public void resolve(Combat c, Character target) {
		self.weaken(5);
		if(target.pet!=null){
			target.pet.caught(c, self);
		}
	}

	@Override
	public Skill copy(Character user) {
		return new FlyCatcher(user);
	}

	@Override
	public Tactics type() {
		return Tactics.summoning;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		// TODO Auto-generated method stub
		return null;
	}

}
