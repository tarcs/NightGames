package skills;

import characters.Character;
import characters.Emotion;
import characters.Trait;

import combat.Combat;
import combat.Result;
import global.Global;

public class Bravado extends Skill {

	public Bravado(Character self) {
		super("Determination", self);
	}

	@Override
	public boolean requirements(Character user) {
		return user.has(Trait.fearless);
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct()&&c.stance.mobile(self)&&self.canSpend(20);
	}

	@Override
	public void resolve(Combat c, Character target) {
		int x = self.getMojo().get();
		if(self.human()){
			c.write(self,deal(x,Result.normal,target));
		}
		else if(target.human()){
			c.write(self,receive(x,Result.normal,target));
		}
		self.spendMojo(x);
		self.calm(x/2,c);
		self.heal(x,c);
		self.emote(Emotion.confident, 30);
		self.emote(Emotion.dominant, 20);
		self.emote(Emotion.nervous,-20);
		self.emote(Emotion.desperate, -30);
	}

	@Override
	public Skill copy(Character user) {
		return new Bravado(user);
	}

	@Override
	public Tactics type() {
		return Tactics.recovery;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		if(Global.random(2)==0){
			return "You refuse to give in and use all your pent up will to keep fighting on.";
		}else{
			return "You grit your teeth and put all your willpower into the fight.";
		}
		
	}

	@Override
	public String receive(int damage, Result modifier, Character attacker) {
		if(Global.random(2)==0){
			return "You can tell by the glint in "+self.name()+"'s eye that she has a renewed sense of purpose in this fight.";
		}
		else{
			return self.name()+" gives you a determined glare as she seems to gain a second wind.";
		}
	}

	@Override
	public String describe() {
		return "Consume mojo to restore stamina and reduce arousal";
	}

}
