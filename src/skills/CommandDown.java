package skills;

import stance.Mount;
import stance.Stance;
import characters.Character;
import combat.Combat;
import combat.Result;

public class CommandDown extends PlayerCommand {

	public boolean usable(Combat c, Character target) {
		return super.usable(c, target) && c.stance.en == Stance.neutral;
	}

	public CommandDown(Character self) {
		super("Force Down", self);
	}

	@Override
	public String describe() {
		return "Command your opponent to lay down on the ground.";
	}

	@Override
	public void resolve(Combat c, Character target) {
		c.stance = new Mount(self, target);
		if (self.human())
			c.write(self,deal(0, Result.normal, target));
		else
			c.write(self,receive(0, Result.normal, target));
	}

	@Override
	public Skill copy(Character user) {
		return new CommandDown(user);
	}


	@Override
	public String deal(int magnitude, Result modifier, Character target) {
		return "Trembling under the weight of your command, " + target.name()
				+ " lies down. You follow her down and mount her, facing her head.";
	}

	@Override
	public String receive(int magnitude, Result modifier, Character target) {
		return "<<This should not be displayed, please inform The"
				+ " Silver Bard: CommandDown-receive>>";
	}

}
