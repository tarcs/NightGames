package skills;

import global.Scheduler;
import items.Attachment;
import items.Toy;
import status.Horny;
import status.Hypersensitive;
import global.Flag;
import global.Global;
import global.Modifier;
import characters.Attribute;
import characters.Character;
import characters.Anatomy;
import characters.Trait;

import combat.Combat;
import combat.Result;

public class Tickle extends Skill {

	public Tickle(Character self) {
		super("Tickle", self);
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct()&&(c.stance.mobile(self)||c.stance.dom(self))&&(c.stance.reachTop(self)||c.stance.reachBottom(self));
	}

	@Override
	public void resolve(Combat c, Character target) {
		Result mod = Result.normal;
		if(c.attackRoll(this, self, target)){
			int level = 0;
			float t = Math.max(2+Global.random(3+target.get(Attribute.Perception))-(target.top.size()+target.bottom.size()),1);
			float w = Math.max(Global.random(3+target.get(Attribute.Perception))-(target.top.size()+target.bottom.size()),1);
			if(target.has(Trait.ticklish)){
				t = Math.round(1.3*t);
				w += Math.round(1.3*t);
			}
			if(self.has(Trait.ticklemonster)&&target.nude()){
					mod = Result.special;
					t *= 1.5;
					w *= 1.5;						
			}
			else if(hastickler()&&Global.random(2)==1&&(!self.human()|| Scheduler.getMatch().condition!=Modifier.notoys)){
				level++;
				if(self.has(Attachment.TicklerFluffy)){
					level ++;
					t *= 1.5;
					w *= 1.5;
				}
				if(target.pantsless()&&c.stance.reachBottom(self)){	
					mod = Result.strong;
					t *= 1.3;
					w *= 1.3;
				}
				else if(target.topless()&&c.stance.reachTop(self)){
					mod = Result.item;
					t *= 1.2;
					w *= 1.2;
				}
				else{
					mod = Result.weak;
					t *= 1.1;
					w *= 1.1;
				}
			}
			else{				
				mod = Result.normal;
			}
			if(self.human()){
				c.write(self,deal(level,mod,target));
			}
			else if(target.human()){
				c.write(self,receive(level,mod,target));
			}
			if(self.has(Toy.Tickler2)&&Global.random(2)==1&&self.canSpend(10)&&(!self.human()||Scheduler.getMatch().condition!=Modifier.notoys)){
				self.spendMojo(10);
				target.add(new Hypersensitive(target),c);
				if(self.human()){
					c.write(self,deal(0,Result.critical,target));
				}
				else if(target.human()){
					c.write(self,receive(0,Result.critical,target));
				}
			}
			if(self.has(Attachment.TicklerPheromones)&&Global.random(4)==1&&(!self.human()||Scheduler.getMatch().condition!=Modifier.notoys)){
				target.add(new Horny(target,6,2),c);
				if(self.human()){
					c.write(self,deal(1,Result.critical,target));
				}
				else if(target.human()){
					c.write(self,receive(1,Result.critical,target));
				}
			}
			if(mod == Result.special){
				target.tempt(Math.round(t),c);
				target.pleasure(1, Anatomy.genitals,c);
				target.weaken(Math.round(w),c);
				self.buildMojo(15);
			}else{
				target.tempt(Math.round(t), Result.foreplay,c);
				target.weaken(Math.round(w),c);
				self.buildMojo(10);
			}
			if(self.human()){
				if(!Global.checkFlag(Flag.exactimages)||target.name().startsWith("Mara")){
					c.offerImage("Tickle.jpg", "Art by AimlessArt");
				}
			}
		}
		else{
			mod = Result.miss;
			if(self.human()){
				c.write(self,deal(0,mod,target));
			}
			else if(target.human()){
				c.write(self,receive(0,mod,target));
			}
		}
		
	}

	@Override
	public boolean requirements(Character user) {
		return !user.has(Trait.cursed);
	}

	@Override
	public Skill copy(Character user) {
		return new Tickle(user);
	}
	public int speed(){
		return 7;
	}
	public Tactics type() {
		return Tactics.pleasure;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		if(modifier == Result.miss){
			return "You try to tickle "+target.name()+", but she squirms away.";
		}
		else if(modifier == Result.special){
			return "You work your fingers across "+target.name()+"'s most ticklish and most erogenous zones until she's a writhing in pleasure and can't even make coherent words.";
		}
		else if(modifier == Result.critical){
			switch(damage){
			case 1: 
				return "She looks confused as her face gradually flushes. The pheromones leaking from your tickler must be affecting her";
			default:
				return "You brush your tickler over "+target.name()+"'s body, causing her to shiver and retreat. When you tickle her again, she yelps and almost falls down. " +
					"It seems like your special feathers made her more sensitive than usual.";
			}
			
		}
		else if(modifier == Result.strong){
			switch(damage){
			case 2:
				return "You attack "+target.name()+"'s sensitive vulva with your fluffy tickler. She shivers and moans at the supernaturally soft sensation.";
			default:
				return "You run your tickler across "+target.name()+"'s sensitive thighs and pussy. She can't help but let out a quiet whimper of pleasure.";
			}
		}
		else if(modifier == Result.item){
			switch(damage){
			case 2:
				return "You tease "+target.name()+"'s bare breasts with your fluffy tickler, causing her to whimper quietly.";
			default:
				return "You tease "+target.name()+"'s naked upper body with your feather tickler, paying close attention to her nipples.";
			}
		}
		else if(modifier == Result.weak){
			switch(damage){
			case 2:
				return "You use your tickler to tease "+target.name()+"'s sensitive ears and neck.";
			default:
				return "You catch "+target.name()+" off guard by tickling her neck and ears.";
			}
		}
		else{
			switch(Global.random(2)){
			case 1:
				return "You give " + target.name() + " a playful smile before launching an all-out tickle attack.";
			default:
				return "You tickle "+target.name()+"'s sides as she giggles and squirms.";
			}
		}
		
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		if(modifier == Result.miss){
			return self.name()+" tries to tickle you, but fails to find a sensitive spot.";
		}
		else if(modifier == Result.special){
			switch(damage){
			case 1:
				
			default:
				return self.name()+" tickles your nude body mercilessly, gradually working her way to your dick and balls. As her fingers start tormenting you privates, you struggle to " +
					"clear your head enough to keep from ejaculating immediately.";
			}
		}
		else if(modifier == Result.special){
			return "You work your fingers across "+target.name()+"'s most ticklish and most erogenous zones until she's a writhing in pleasure and can't even make coherent words.";
		}
		else if(modifier == Result.critical){
			switch(damage){
			case 1:
				return "You gradually feel a soft heat spread from where you were tickled. You must be affected by an aphrodisiac.";
			default:
				return "After she stops, you feel an unnatural sensitivity where it touched touched you.";
			}
		}
		else if(modifier == Result.strong){
			switch(damage){
			case 2:
				return self.name()+" rubs her mystically soft tickler over your sensitive dick and balls.";
			default:
				return self.name()+" brushes her tickler over your balls and teases the sensitive head of your penis.";
			}
		}
		else if(modifier == Result.item){
			switch(damage){
			case 2:
				return self.name()+" teases your bare upper body with her fluffy tickler.";
			default:
				return self.name()+" runs her feather tickler across your nipples and abs.";
			}
		}
		else if(modifier == Result.weak){
			switch(damage){
			case 2:
				return self.name()+" attacks your exposed skin with a fluffy tickler.";
			default:
				return self.name()+" pulls out a feather tickler and teases any exposed skin she can reach.";
			}
		}
		else{
			return self.name()+" suddenly springs toward you and tickles you relentlessly until you can barely breathe.";
		}
	}

	@Override
	public String describe() {
		return "Tickles opponent, weakening and arousing her. More effective if she's nude";
	}
	private boolean hastickler(){
		return self.has(Toy.Tickler)||self.has(Toy.Tickler2);
	}
}
