package skills;

import characters.Anatomy;
import characters.Attribute;
import characters.Character;
import combat.Combat;
import combat.Result;
import status.Horny;
import status.Stsflag;

public class KissBetter extends Skill{
    public KissBetter(Character self) {
        super("Kiss Better", self);
    }

    @Override
    public boolean requirements(Character user) {
        return user.getPure(Attribute.Footballer)>=15;
    }

    @Override
    public boolean usable(Combat c, Character target) {
        return target.pantsless()&&c.stance.oral(self)&&!c.stance.behind(self)&&self.canAct()&&!c.stance.penetration(self)
                &&target.is(Stsflag.stunned);
    }

    @Override
    public String describe() {
        return "Lick and suck a stunned opponent's genitals to make them feel better.";
    }

    @Override
    public void resolve(Combat c, Character target) {
        if(self.human()){
            if(target.hasBalls()){
                c.write(self,deal(0,Result.strong,target));
            }else{
                c.write(self,deal(0,Result.normal,target));
            }
        }
        else if(target.human()){
            c.write(self,receive(0,Result.normal,target));
        }
        target.pleasure(self.get(Attribute.Footballer)+(2*self.get(Attribute.Seduction)/3)+target.get(Attribute.Perception), Anatomy.genitals,c);
        target.heal(target.getStamina().max()/3);
        target.add(new Horny(target,2,8),c);
    }

    @Override
    public Skill copy(Character user) {
        return new KissBetter(user);
    }

    @Override
    public Tactics type() {
        return Tactics.pleasure;
    }

    @Override
    public String deal(int damage, Result modifier, Character target) {
        if(modifier==Result.strong){
            return "You lean over "+target.name()+"'s unresisting body and suck on her tender balls. She coos with pleasure as her pain is replaced with arousal.";
        }
        return "You lean over "+target.name()+"'s unresisting body and lavish her exposed pussy with licking and sucking. She coos with pleasure as her pain is replaced with arousal.";
    }

    @Override
    public String receive(int damage, Result modifier, Character target) {
        return self.name()+" gives your naked balls a sloppy kiss before taken them into her mouth one at a time and sucking them. " +
                "You feel better, both in the oral pleasure sense, and the sense of your pain easing.";
    }
}
