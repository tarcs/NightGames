package skills;

import characters.Attribute;
import characters.Character;
import characters.Emotion;
import combat.Combat;
import combat.Result;
import status.Stsflag;

public class PheromoneOverdrive extends Skill {

	public PheromoneOverdrive(Character self) {
		super("Pheromone Pink Overdrive", self);
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Animism)>=30;
	}


	@Override
	public boolean usable(Combat c, Character target) {
		return c.stance.dom(self)&&self.canAct()&&self.is(Stsflag.feral)
				&&self.canSpend(30)&&(self.getArousal().percent()>target.getArousal().percent());
	}

	@Override
	public String describe() {
		return "Use an intense burst of pheromones to make your opponent as horny as you are: 30 mojo";
	}

	@Override
	public void resolve(Combat c, Character target) {
		self.spendMojo(30);
		target.getArousal().set((self.getArousal().percent()*target.getArousal().max())/100);
		if(self.human()){
			c.write(self,deal(0,Result.normal,target));
		}else if(target.human()){
			c.write(self,receive(0,Result.normal,target));
		}
		target.emote(Emotion.horny, 30);
	}

	@Override
	public Skill copy(Character user) {
		return new PheromoneOverdrive(user);
	}

	@Override
	public Tactics type() {
		return Tactics.pleasure;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		return String.format("You take a deep breath to focus all your primal pheromones into a thick aura. You grab %s and give %s "
				+ "a passionate kiss while you channel your pheromones into %s body, instantly driving %s into heat.", 
				target.name(),target.pronounTarget(false),target.pronounTarget(false),target.pronounTarget(false));
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		return String.format("%s seems to glow with pink energy. It can't be! Is that a cloud of pheromones so dense it's visible to "
				+ "the naked eye? Before you can escape, %s pulls you into a deep kiss. With your mouth occupied, you're forced to "
				+ "breathe %s heady scent through your nose. The pheromone-filled smell overwhelms you and makes your cock painfully hard.", 
				self.name(),self.pronounSubject(false),self.possessive(false));
	}

}
