package skills;

import global.Global;
import combat.Combat;
import combat.Result;
import characters.Attribute;
import characters.Character;
import characters.Anatomy;

public class CommandHurt extends PlayerCommand {

	public CommandHurt(Character self) {
		super("Force Pain", self);
	}

	@Override
	public String describe() {
		return "Convince your thrall running into the nearest wall is a good idea.";
	}

	@Override
	public void resolve(Combat c, Character target) {
		if(Global.random(2)==0){
			target.pain(Global.random(10) + target.get(Attribute.Power)/2,Anatomy.genitals,c);
			c.write(self,deal(0, Result.strong, target));
		}else{
			target.pain(Global.random(10) + target.get(Attribute.Speed),Anatomy.head,c);
			c.write(self,deal(0, Result.normal, target));
		}

	}

	@Override
	public Skill copy(Character user) {
		return new CommandHurt(user);
	}

	@Override
	public String deal(int magnitude, Result modifier, Character target) {
		if( modifier == Result.strong){
			return " You glare at "+target.name()+" and point towards your own groin with a nod.  "
					+ target.name() +"'s hand reaches out in front of them, then they swiftly and firmly slap themselves in the privates.";
		}
		return "Grinning, you point towards the nearest wall. " + target.name()
				+ " seems confused for a moment, but soon she understands your"
				+ " meaning and runs headfirst into it.";
	}

	@Override
	public String receive(int magnitude, Result modifier, Character target) {
		return "<<This should not be displayed, please inform The"
				+ " Silver Bard: CommandHurt-receive>>";
	}

}
