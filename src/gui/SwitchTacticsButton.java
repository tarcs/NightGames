package gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.border.LineBorder;

import global.Global;
import skills.TacticGroup;
import skills.Tactics;

public class SwitchTacticsButton extends KeyableButton {
    private static final long serialVersionUID = -3949203523669294068L;

    private TacticGroup group;
    public SwitchTacticsButton(TacticGroup group) {
        super(group.name());
        setBorderPainted(false);
        setOpaque(true);
        this.group = group;
        Color bgColor = new Color(80, 220, 120);
        for (Tactics tactic : Tactics.values()) {
            if (tactic.getGroup() == group) {
                bgColor = tactic.getColor();
                break;
            }
        }

        setBackground(bgColor);
        setMinimumSize(new Dimension(0, 20));
        setForeground(foregroundColor(bgColor));
        setFont(new Font("Georgia", 0, 16));
        setBorder(new LineBorder(getBackground(), 3));
        int nSkills = Global.gui().nSkillsForGroup(group);
        setText(group.name() + " [" + nSkills + "]");
        if (nSkills == 0 && group != TacticGroup.All) {
            setEnabled(false);
            setForeground(Color.WHITE);
            setBackground(getBackground().darker());
        }

        addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
			    Global.gui().switchTactics(SwitchTacticsButton.this.group);
			}
		});
    }

    private static Color foregroundColor(Color bgColor) {
        float hsb[] = new float[3];
        Color.RGBtoHSB(bgColor.getRed(), bgColor.getGreen(), bgColor.getRed(), hsb);
        if (hsb[2] < .6) {
            return Color.WHITE;
        } else {
            return Color.BLACK;
        }
    }
}