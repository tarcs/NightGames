package items;

import java.util.ArrayList;

import characters.Character;

public interface Item extends Loot{
	
	public String getDesc();
	public String getFullDesc(Character owner);
	public int getPrice();
	public String getName();
	public String getFullName(Character owner);
	public ArrayList<Item> getRecipe();
	public String pre();
	public Boolean listed();
	@Override
	public void pickup(Character owner);
}
