package global;

import java.io.*;
import java.time.LocalTime;
import java.util.Scanner;

import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;

import characters.*;
import characters.Character;
import com.google.gson.*;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;

public class SaveManager {
	
	public static void save(boolean auto){
        FileWriter file;
        try {
            if (auto) {
                file = new FileWriter("./auto.sav");
            } else {
                JFileChooser dialog = new JFileChooser("./");
                dialog.setMultiSelectionEnabled(false);
                dialog.setFileFilter(new FileNameExtensionFilter("Save File (.sav)", "sav"));
                dialog.setSelectedFile(new File("Save.sav"));
                int rv = dialog.showSaveDialog(Global.gui());

                if (rv != JFileChooser.APPROVE_OPTION) {
                    return;
                }
                file = new FileWriter(dialog.getSelectedFile());
            }

            JsonObject saver = new JsonObject();
            saver.add("Roster", Roster.Save());

            saver.addProperty("Date", Scheduler.getDate());
            saver.addProperty("Time", Scheduler.getTime().getHour());
            saver.addProperty("Match", Scheduler.getMatchNumber());

            JsonArray flags = new JsonArray();
            for (Flag f : Global.getFlags()) {
                flags.add(f.name());
            }
            saver.add("Flags", flags);

            JsonObject counters = new JsonObject();
            for (Flag f : Global.getCounters().keySet()) {
                counters.addProperty(f.name(), Global.getValue(f));
            }
            saver.add("Counters", counters);

            file.write(saver.toString());
            file.flush();
            file.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

	public static void load(){
		JFileChooser dialog = new JFileChooser("./");
		dialog.setMultiSelectionEnabled(false);
		dialog.setFileFilter(new FileNameExtensionFilter("Save File (.sav)","sav"));
		int rv = dialog.showOpenDialog(Global.gui());
		if (rv != JFileChooser.APPROVE_OPTION)
		{
			return;
		}

		Global.reset();

        Global.buildSkillPool(Global.getPlayer());
        FileReader file;
		try {
			file = new FileReader(dialog.getSelectedFile());
            JsonParser loader = new JsonParser();
            JsonObject data = loader.parse(file).getAsJsonObject();

            Roster.Load( data.getAsJsonObject("Roster"));

            Scheduler.setDate(data.get("Date").getAsInt());
            Scheduler.setTime(LocalTime.of(data.get("Time").getAsInt(),0));
            Scheduler.setMatchNum(data.get("Match").getAsInt());

            JsonArray flags = data.getAsJsonArray("Flags");
            for(JsonElement f:flags){
                if(Flag.valueOf(f.getAsString())!=null){
                    Global.flag(Flag.valueOf(f.getAsString()));
                }
            }

            JsonObject counters = data.getAsJsonObject("Counters");
            for(String key: counters.keySet()){
                if(Flag.valueOf(key)!=null){
                    Global.setCounter(Flag.valueOf(key),counters.get(key).getAsFloat());
                }
            }

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (JsonParseException e){
		    try{
		        load_legacy(new FileInputStream(dialog.getSelectedFile()));
            } catch (FileNotFoundException e1) {
                e1.printStackTrace();
            }
        }
        Global.setPlayer(Roster.get(ID.PLAYER));
        Global.gui().populatePlayer((Player) Roster.get(ID.PLAYER));
        if(Scheduler.getTime().getHour()>=22){
            Scheduler.dusk();
        }
        else{
            Scheduler.dawn();
        }

	}

	public static void load_legacy(FileInputStream file){
		String header;
		boolean dawn = false;

			Scanner loader = new Scanner(file);
			while(loader.hasNext()){
				header=loader.next();				
				if(header.equalsIgnoreCase("P")){
					Global.getPlayer().load(loader);
				}
				else if(header.equalsIgnoreCase("NPC")){
					String name = loader.next();
					Roster.get(name).load(loader);
				}
				else if(header.equalsIgnoreCase("Flags")){
					String next = loader.next();
					while(!next.equals("#")){
						Global.flag(Flag.valueOf(next));
						next = loader.next();
					}
					if(loader.next().equalsIgnoreCase("Dawn")){
						dawn=true;
					}
					else{
						dawn=false;
					}
					if(loader.hasNext()){
						Scheduler.setDate(loader.nextInt());
					}
					else{
						Scheduler.setDate(1);
					}
					while(loader.hasNext()){
						Global.setCounter(Flag.valueOf(loader.next()),Float.parseFloat(loader.next()));
					}
				}
			}
			loader.close();

		if(dawn){
			Scheduler.setTime(LocalTime.of(15,0));
		}
		else{
            Scheduler.setTime(LocalTime.of(22,0));
		}
	}

}
