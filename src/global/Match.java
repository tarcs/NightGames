package global;

import characters.*;
import characters.Character;
import items.Item;
import items.Trophy;

import java.awt.Rectangle;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import status.Hypersensitive;
import status.Stsflag;
import actions.Movement;
import areas.Area;
import areas.Cache;
import areas.MapDrawHint;

public class Match {
	private int dropOffTime;
	private int cacheDelay;
	private LocalTime matchend;
	private HashMap<Movement,Area> map;
	public ArrayList<Character> combatants;
	private HashMap<Character,Integer> score;
    private HashMap<Character,Integer> clothingscore;
	private int index;
	private boolean pause;
	public Modifier condition;
	private boolean quit;
	
	public Match(Collection<Character> combatants, Modifier condition){
		this.score=new HashMap<Character,Integer>();
		this.condition=condition;
		this.combatants = new ArrayList<Character>();
		this.combatants.addAll(combatants);
		matchend = LocalTime.of(1,00);
		if(Global.checkFlag(Flag.shortmatches)){
			matchend = LocalTime.of(0,00);
		}
		if(Global.checkFlag(Flag.MoreCache1)){
			cacheDelay = 4;
		}else{
			cacheDelay = 6;
		}
		map = Match.buildMap();
		Global.gui().startMatch();
		dropOffTime = 0;
		pause=false;
		quit = false;
		this.combatants.get(0).place(map.get(Movement.dorm));
		this.combatants.get(1).place(map.get(Movement.engineering));
		if(this.combatants.size()>=3){
			this.combatants.get(2).place(map.get(Movement.la));
		}
		if(this.combatants.size()>=4){
			this.combatants.get(3).place(map.get(Movement.dining));
		}
		if(this.combatants.size()>=5){
			this.combatants.get(4).place(map.get(Movement.union));
		}
		for(Character combatant:combatants){
			score.put(combatant, 0);
			combatant.matchPrep(this);
		}
	}
	public void automate(LocalTime until){
        while(!Scheduler.getTime().equals(until)){
            if(index>=combatants.size()){
                index=0;
                if(meanLvl()>5&&dropOffTime>=cacheDelay){
                    dropPackage();
                    dropOffTime = 0;
                }
                if(Global.checkFlag(Flag.challengeAccepted)&&(Scheduler.getTime().getMinute()%15==0)){
                    dropChallenge();
                }
                Scheduler.advanceTime(LocalTime.of(0,5));
                dropOffTime++;
            }
            while(index<combatants.size()){
                if(combatants.get(index).state!=State.quit){
                    combatants.get(index).upkeep();
                    if(combatants.get(index).human()){
                        manageConditions(combatants.get(index));
                    }
                    combatants.get(index).move(this);
                    if(Global.debug){
                        System.out.println(combatants.get(index).name()+" is in "+combatants.get(index).location().name);
                    }
                }
                index++;
            }
        }
        //end();
    }

	public void round(){
		while(Scheduler.getTime().isBefore(matchend) || Scheduler.getTime().isAfter(LocalTime.of(20,0))|| Global.getPlayer().isBusy()){
			if(index>=combatants.size()){
				index=0;
				if(meanLvl()>5&&dropOffTime>=cacheDelay){
					dropPackage();
					dropOffTime = 0;
				}
				if(Global.checkFlag(Flag.challengeAccepted)&&(Scheduler.getTime().getMinute()%15==0)){
					dropChallenge();
				}
				Scheduler.advanceTime(LocalTime.of(0,5));
				dropOffTime++;
			}
			while(index<combatants.size()){
				Global.gui().refresh();
				if(combatants.get(index).state!=State.quit){
					combatants.get(index).upkeep();
					if(combatants.get(index).human()){
						manageConditions(combatants.get(index));
					}
					combatants.get(index).move(this);
					if(Global.debug){
						System.out.println(combatants.get(index).name()+" is in "+combatants.get(index).location().name);
					}
				}
				index++;
				if(pause){
					return;
				}
			}
		}
		end();
	}
	public void pause(){
		pause=true;
	}
	public void resume(){
		pause=false;
		round();
	}
	public void end(){
		for(Character next:combatants){
			next.finishMatch();
		}
		if(condition!=Modifier.quiet || quit) {
            Global.gui().message("Tonight's match is over.");
        }
		int cloth=0;
		int creward=0;
		int trophies;
		clothingscore = new HashMap<Character,Integer>();
        HashMap<Character,Integer> clothingmoney = new HashMap<Character,Integer>();
		Character player=null;
		for(Character combatant:combatants){
            trophies = 0;
            cloth = 0;
            for(Trophy t: Trophy.values()){
                while(combatant.has(t)){
                    combatant.remove(t,1);
                    cloth += combatant.income(combatant.prize());
                    trophies++;
                }
            }
            clothingscore.put(combatant,trophies);
            clothingmoney.put(combatant,cloth);
        }
        ArrayList<Character> order = rankParticipants(score);
        HashMap<Character,Integer> winsmoney = new HashMap<Character,Integer>();
        HashMap<Character,Integer> challengemoney = new HashMap<Character,Integer>();
		for(Character combatant:order){
            if(condition!=Modifier.quiet || quit) {
                Global.gui().message(combatant.name() + " scored " + score.get(combatant) + " victories.");
            }
			winsmoney.put(combatant,combatant.income(score.get(combatant)*combatant.prize()));
			if(combatant.human()){
				player=combatant;
			}
			creward = 0;
			for(Challenge c: combatant.challenges){
				if(c.done){
                    creward += c.reward()+(c.reward()*3*combatant.getRank());
                    if(combatant.has(Trait.bountyHunter)){
                        creward *= 2;
                    }
                    if(combatant.human()) {
                        Global.modCounter(Flag.ChallengesCompleted, 1);
                    }
				}
			}
            creward = combatant.income(creward);
			challengemoney.put(combatant,creward);
			combatant.challenges.clear();
			combatant.state=State.ready;
			combatant.change(Modifier.normal);
			
		}
		if(condition!=Modifier.quiet || quit) {
            Global.gui().message("You made $" + winsmoney.get(player) + " for defeating opponents.");
            if (!Global.checkFlag(Flag.challengemode)) {
                int bonus = player.income(Math.round(score.get(player) * player.prize() * condition.bonus()));
                if (bonus > 0) {
                    Global.gui().message("You earned an additional $" + bonus + " for accepting the handicap.");
                }
            }
            if (order.get(0) == player) {
                Global.gui().message("You also earned a bonus of $" + 5 * player.prize() + " for placing first.");
                Global.flag(Flag.victory);
                Global.modCounter(Flag.MatchWins, 1);
            }
            Global.gui().message("You traded in " + clothingscore.get(player) + " sets of clothes for a total of $" + clothingmoney.get(player) + ".");
            if (creward > 0) {
                Global.gui().message("You also discover an envelope with $" + challengemoney.get(player) + " slipped under the door to your room. Presumably it's payment for completed challenges.");
            }
            Global.gui().message("<p>");
        }
		if(Scheduler.getMatchNumber()<=Constants.SEASONLENGTH) {
            order.get(0).income(5 * order.get(0).prize());
            Scheduler.addScore(order.get(0).id(), 5);
            Scheduler.addScore(order.get(1).id(), 3);
            Scheduler.addScore(order.get(2).id(), 2);
            Scheduler.addScore(order.get(3).id(), 1);
            if(Scheduler.getMatchNumber()==Constants.SEASONLENGTH) {
                Global.gui().message("The season has concluded. There will be a small award ceremony for the champion tomorrow morning.");
            }else{
                Global.gui().message("There are still "+(Constants.SEASONLENGTH-Scheduler.getMatchNumber())+" matches left in the season.<br>" +
                        "The new rankings are:<br>"+Scheduler.displayScores());
            }
        }else{
		    Global.gui().message("This match wasn't part of a season, so the results do not matter beyond tonight's prize. The new season will start with a clean slate next week.");
        }
        Scheduler.incMatchNum();
		new Postmatch(player, combatants);
	}
	private ArrayList<Character> rankParticipants(HashMap<Character, Integer> score) {
		ArrayList<Character> result = new ArrayList<Character>();
		boolean inserted;
		for(Character combatant: score.keySet()){
			inserted = false;
			for(int i=0;i<result.size();i++){
				if(score.get(combatant) > score.get(result.get(i)) ||
                        (score.get(combatant) == score.get(result.get(i)) && clothingscore.get(combatant) > clothingscore.get(result.get(i)))){
					result.add(i,combatant);
					inserted = true;
					break;
				}
			}
			if(!inserted){
				result.add(combatant);
			}		
		}
		return result;
	}
	public Area gps(Movement dest){
		if(map.containsKey(dest)){
			return map.get(dest);
		}
		return null;
	}
	public void score(Character character, int points) {
		score.put(character, score.get(character)+points);
	}
	public void manageConditions(Character player){
		if(condition==Modifier.vibration){
			player.tempt(5);
		}
	}
	public int meanLvl(){
		int mean = 0;
		for(Character player: combatants){
			mean+=player.getLevel();
		}
		return mean/combatants.size();
	}
	public void dropPackage(){
		ArrayList<Area> areas = new ArrayList<Area>();
		areas.addAll(map.values());
		boolean placed = false;
		while(!placed){
			Area target = areas.get(Global.random(areas.size()));
			if(!target.corridor()&&!target.open()&&target.env.size()<5){
				target.place(new Cache(Global.getPlayer().getRank()));
				if(Global.checkFlag(Flag.CacheFinder) && condition!=Modifier.quiet){
					Global.gui().message("You receive a text that a new cache has been placed in "+target.name);
				}
				placed = true;
			}
		}
	}
	public void dropChallenge(){
		ArrayList<Area> areas = new ArrayList<Area>();
		areas.addAll(map.values());
		boolean placed = false;
		while(!placed) {
			Area target = areas.get(Global.random(areas.size()));
			if (!target.open() && target.env.size() < 5) {
				target.place(new Challenge(this));
				placed = true;
			}
		}
	}
	public void quit() {
	    Global.gui().clearText();
		Character human = Roster.get(ID.PLAYER);
		if(human.state==State.combat){
			if(human.location().fight.getCombat()!=null){
				human.location().fight.getCombat().forfeit(human);
			}
			human.location().endEncounter();
		}
		human.travel(new Area("","",Movement.retire));
		human.state=State.quit;
		condition = Modifier.quiet;
		quit = true;
		resume();
	}
	public Collection<Area> getAreas() {
		return map.values();
	}
	public static HashMap<Movement,Area> buildMap(){
		Area quad = new Area("Quad",
                "You are in the <b>Quad</b> that sits in the center of the Dorm, the Dining Hall, the Engineering Building, and the Liberal Arts Building. There's "
                                + "no one around at this time of night, but the Quad is well-lit and has no real cover. You can probably be spotted from any of the surrounding buildings, so it may "
                                + "not be a good idea to hang out here for long.",
                Movement.quad, new MapDrawHint(new Rectangle(10, 3, 7, 9), "Quad", false));
		Area dorm = new Area("Dorm",
                "You are in the <b>Dorm</b>. Everything is quieter than it would be in any other dorm this time of night. You've been told the entire first floor "
                                + "is empty during match hours, but you wouldn't be surprised if a few of the residents are hiding in their rooms, peeking at the fights. You've stashed some clothes "
                                + "in one of the rooms you're sure is empty, which is common practice for most of the competitors.",
                Movement.dorm, new MapDrawHint(new Rectangle(14, 12, 3, 5), "Dorm", false));
		Area shower = new Area("Showers",
                "You are in the first floor <b>Showers</b>. There are a half-dozen stalls shared by the residents on this floor. They aren't very big, but there's "
                                + "room to hide if need be. A hot shower would help you recover after a tough fight, but you'd be vulnerable if someone finds you.",
                Movement.shower, new MapDrawHint(new Rectangle(13, 17, 4, 2), "Showers", false));
		Area laundry = new Area("Laundry Room",
                "You are in the <b>Laundry Room</b> in the basement of the Dorm. Late night is prime laundry time in your dorm, but none of these machines "
                                + "are running. You're a bit jealous when you notice that the machines here are free, while yours are coin-op. There's a tunnel here that connects to the basement of the "
                                + "Dining Hall.",
                Movement.laundry, new MapDrawHint(new Rectangle(17, 15, 8, 2), "Laundry", false));
		Area engineering = new Area("Engineering Building",
                "You are in the Science and <b>Engineering Building</b>. Most of the lecture rooms are in other buildings; this one is mostly "
                                + "for specialized rooms and labs. The first floor contains workshops mostly used by the Mechanical and Electrical Engineering classes. The second floor has "
                                + "the Biology and Chemistry Labs. There's a third floor, but that's considered out of bounds.",
                Movement.engineering, new MapDrawHint(new Rectangle(10, 0, 7, 3), "Eng", false));
		Area lab = new Area("Chemistry Lab",
                "You are in the <b>Chemistry Lab</b>. The shelves and cabinets are full of all manner of dangerous and/or interesting chemicals. A clever enough "
                                + "person could combine some of the safer ones into something useful. Just outside the lab is a bridge connecting to the library.",
                Movement.lab, new MapDrawHint(new Rectangle(0, 0, 10, 3), "Lab", false));
		Area workshop = new Area("Workshop",
                "You are in the Mechanical Engineering <b>Workshop</b>. There are shelves of various mechanical components and the back table is covered "
                                + "with half-finished projects. A few dozen Mechanical Engineering students use this workshop each week, but it's well stocked enough that no one would miss "
                                + "some materials that might be of use to you.",
                Movement.workshop, new MapDrawHint(new Rectangle(17, 0, 8, 3), "Workshop", false));
		Area libarts = new Area("Liberal Arts Building",
                "You are in the <b>Liberal Arts Building</b>. There are three floors of lecture halls and traditional classrooms, but only "
                                + "the first floor is in bounds. The Library is located directly out back, and the side door is just a short walk from the pool.",
                Movement.la, new MapDrawHint(new Rectangle(5, 5, 5, 7), "Lib Arts", false));
		Area pool = new Area("Pool",
                "You are by the indoor <b>Pool</b>, which is connected to the Student Union for reasons that no one has ever really explained. The pool is quite "
                                + "large and there is even a jacuzzi. A quick soak would feel good, but the lack of privacy is a concern. The side doors are locked at this time of night, but the "
                                + "door to the Student Union is open and there's a back door that exits near the Liberal Arts building. Across the water in the other direction is the Courtyard.",
                Movement.pool, new MapDrawHint(new Rectangle(6, 12, 4, 2), "Pool", false));
		Area library = new Area("Library",
                "You are in the <b>Library</b>. It's a two floor building with an open staircase connecting the first and second floors. The front entrance leads to "
                                + "the Liberal Arts building. The second floor has a Bridge connecting to the Chemistry Lab in the Science and Engineering building.",
                Movement.library, new MapDrawHint(new Rectangle(0, 8, 5, 12), "Library", false));
		Area dining = new Area("Dining Hall",
                "You are in the <b>Dining Hall</b>. Most students get their meals here, though some feel it's worth the extra money to eat out. The "
                                + "dining hall is quite large and your steps echo on the linoleum, but you could probably find someplace to hide if you need to.",
                Movement.dining, new MapDrawHint(new Rectangle(17, 6, 4, 6), "Dining", false));
		Area kitchen = new Area("Kitchen",
                "You are in the <b>Kitchen</b> where student meals are prepared each day. The industrial fridge and surrounding cabinets are full of the "
                                + "ingredients for any sort of bland cafeteria food you can imagine. Fortunately, you aren't very hungry. There's a chance you might be able to cook up some "
                                + "of the more obscure items into something useful.",
                Movement.kitchen, new MapDrawHint(new Rectangle(18, 12, 4, 2), "Kitchen", false));
		Area storage = new Area("Storage Room",
                "You are in a <b>Storage Room</b> under the Dining Hall. It's always unlocked and receives a fair bit of foot traffic from students "
                                + "using the tunnel to and from the Dorm, so no one keeps anything important in here. There's enough junk down here to provide some hiding places and there's a chance "
                                + "you could find something useable in one of these boxes.",
                Movement.storage, new MapDrawHint(new Rectangle(21, 6, 4, 5), "Storage", false));
		Area tunnel = new Area("Tunnel",
                "You are in the <b>Tunnel</b> connecting the dorm to the dining hall. It doesn't get a lot of use during the day and most of the freshmen "
                                + "aren't even aware of its existence, but many upperclassmen have been thankful for it on cold winter days and it's proven to be a major tactical asset. The "
                                + "tunnel is well-lit and doesn't offer any hiding places.",
                Movement.tunnel, new MapDrawHint(new Rectangle(23, 11, 2, 4), "Tunnel", true));
		Area bridge = new Area("Bridge",
                "You are on the <b>Bridge</b> connecting the second floors of the Science and Engineering Building and the Library. It's essentially just a "
                                + "corridor, so there's no place for anyone to hide.",
                Movement.bridge, new MapDrawHint(new Rectangle(0, 3, 2, 5), "Bridge", true));
		Area sau = new Area("Student Union",
                "You are in the <b>Student Union</b>, which doubles as base of operations during match hours. You and the other competitors can pick up "
                                + "a change of clothing here.",
                Movement.union, new MapDrawHint(new Rectangle(10, 12, 3, 5), "S.Union", true));
		quad.link(dorm);
		quad.link(engineering);
		quad.link(libarts);
		quad.link(dining);
		quad.link(sau);
		dorm.link(shower);
		dorm.link(laundry);
		dorm.link(quad);
		shower.link(dorm);
		laundry.link(dorm);
		laundry.link(tunnel);
		engineering.link(quad);
		engineering.link(lab);
		engineering.link(workshop);
		workshop.link(engineering);
		workshop.shortcut(pool);
		lab.link(engineering);
		lab.link(bridge);
		lab.jump(dining);
		libarts.link(quad);
		libarts.link(library);
		libarts.link(pool);
		pool.link(libarts);
		pool.link(sau);
		pool.shortcut(workshop);
		library.link(libarts);
		library.link(bridge);
		library.shortcut(tunnel);
		dining.link(quad);
		dining.link(storage);
		dining.link(kitchen);
		kitchen.link(dining);
		storage.link(dining);
		storage.link(tunnel);
		tunnel.link(storage);
		tunnel.link(laundry);
		tunnel.shortcut(library);
		bridge.link(lab);
		bridge.link(library);
		bridge.jump(quad);
		sau.link(pool);
		sau.link(quad);
		HashMap<Movement,Area> map = new HashMap<Movement, Area>();
		map.put(Movement.quad,quad);
		map.put(Movement.dorm,dorm);
		map.put(Movement.shower,shower);
		map.put(Movement.laundry, laundry);
		map.put(Movement.engineering, engineering);
		map.put(Movement.workshop,workshop);
		map.put(Movement.lab,lab);
		map.put(Movement.la,libarts);
		map.put(Movement.pool,pool);
		map.put(Movement.library,library);
		map.put(Movement.dining,dining);
		map.put(Movement.kitchen,kitchen);
		map.put(Movement.storage,storage);
		map.put(Movement.tunnel,tunnel);
		map.put(Movement.bridge,bridge);
		map.put(Movement.union, sau);
		return map;
	}

}
