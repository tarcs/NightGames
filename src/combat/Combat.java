package combat;
import characters.*;
import characters.Character;
import global.Flag;
import global.Global;

import items.Clothing;
import items.ClothingType;
import items.Item;
import items.Potion;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Observable;

import pet.Pet;

import skills.Skill;
import stance.Neutral;
import stance.Position;
import stance.Stance;
import stance.StandingOver;
import status.Status;
import status.Winded;

import areas.Area;


public class Combat extends Observable implements Serializable, Cloneable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -8279523341570263846L;
	public Character p1;
	public Character p2;
	public int phase;
	private HashMap<Character,Skill> actions;
	private HashMap<Character,Boolean> lowblow;
	private ArrayList<Character> audience;
	public Area location;
	private String message;
	public Position stance;
	public ArrayList<Clothing> clothespile;
	public HashMap<Character,ArrayList<Item>> used;
	private int timer;
	public Result state;
	private HashMap<String,String> images;
	
	public Combat(Character p1, Character p2, Area loc){
		this.p1=p1;
		this.p2=p2;
		location=loc;
		stance = new Neutral(p1,p2);
		message = "";
		audience = new ArrayList<Character>();
		clothespile=new ArrayList<Clothing>();
		used = new HashMap<Character,ArrayList<Item>>();
		used.put(p1, new ArrayList<Item>());
		used.put(p2, new ArrayList<Item>());
		actions = new HashMap<Character,Skill>();
		lowblow = new HashMap<Character,Boolean>();
		lowblow.put(p1, false);
		lowblow.put(p2, false);
		timer=0;
		images = new HashMap<String,String>();
		p1.state=State.combat;
		p2.state=State.combat;
		p1.preCombat(p2,this);
		p2.preCombat(p1,this);
	}
	public Combat(Character p1, Character p2, Area loc, Position starting){
		this(p1,p2,loc);
		stance=starting;
	}
	public Combat(Character p1, Character p2, Area loc,int code){
		this(p1,p2,loc);
		switch(code){
			case 1:
				p2.undress(this);
				p1.emote(Emotion.dominant, 50);
				p2.emote(Emotion.nervous, 50);
			default:
		}
	}
	public void go(){
		phase=0;
		if(p1.nude()){
			p1.emote(Emotion.nervous, 20);
		}
		if(p2.nude()){
			p2.emote(Emotion.nervous, 20);
		}
		if(!(p1.human()||p2.human())){
			automate();
		}
		this.setChanged();
		this.notifyObservers();
	}
	public void turn(){
		if(p1.getArousal().isFull()&&p2.getArousal().isFull()){
			state = eval(null);
			p1.evalChallenges(this, null);
			p2.evalChallenges(this, null);
			p2.draw(this,state);
			for(Character viewer: audience){
				if(Global.random(2)==0){
					viewer.watcher(this, p1, p2);
				}else{
					viewer.watcher(this, p2, p1);
				}				
			}
			phase=2;
			this.setChanged();
			this.notifyObservers();
			if(!(p1.human()||p2.human())){
				end();
			}
			return;
		}
		if(p1.getArousal().isFull()){
			state = eval(p2);
			p1.evalChallenges(this, p2);
			p2.evalChallenges(this, p2);
			p2.victory(this,state);
			for(Character viewer: audience){
				viewer.watcher(this, p2, p1);				
			}
			phase=2;
			this.setChanged();
			this.notifyObservers();
			if(!(p1.human()||p2.human())){
				end();
			}
			return;
		}
		if(p2.getArousal().isFull()){
			state = eval(p1);
			p1.evalChallenges(this, p1);
			p2.evalChallenges(this, p1);
			p1.victory(this,state);
			for(Character viewer: audience){
				viewer.watcher(this, p1, p2);				
			}
			phase=2;
			this.setChanged();
			this.notifyObservers();
			if(!(p1.human()||p2.human())){
				end();
			}
			return;
		}
		
		message = p2.describe(p1.get(Attribute.Perception))+"<p>"+stance.describe()+"<p>"+p1.describe(p2.get(Attribute.Perception))+"<p>";
		if (p1.human() || p2.human()) {
			NPC commenter = (NPC) getOther(Global.getPlayer());
			String comment = commenter.getComment(this);
			if (comment!=null) {
				write(commenter,  comment );
			}
		}
		phase=1;
		p1.regen(true);
		p2.regen(true);
		lowblow.put(p1, false);
		lowblow.put(p2, false);
		actions.put(p1, null);
		actions.put(p2, null);
		p1.act(this);		
		p2.act(this);

		//}
		this.setChanged();
		this.notifyObservers();
	}
	
	public Character getOther(Character player) {
		if(p1==player){
			return p2;
		}
		else{
			return p1;
		}
	}
	public Result eval(Character victor) {
		if(victor!=null){
			if((stance.en==Stance.anal||stance.en==Stance.analm)){
				return Result.anal;
			}
			if((stance.en==Stance.facesitting)){
				return Result.facesitting;
			}
		}
		if(stance.penetration(p1)||stance.penetration(p2)){
			return Result.intercourse;
		}
		else{
			return Result.normal;
		}
	}
	public void act(Character c, Skill action){
		actions.put(c, action);
		if(actions.get(p1)!=null&&actions.get(p2)!=null){
			clear();
			if(Global.debug){
				System.out.println(p1.name()+" uses "+actions.get(p1).toString());
				System.out.println(p2.name()+" uses "+actions.get(p2).toString());
			}
			if(p1.pet!=null&&p2.pet!=null){
				petbattle(p1.pet,p2.pet);
			}
			else if(p1.pet!=null){
				p1.pet.act(this, p2);
			}
			else if(p2.pet!=null){
				p2.pet.act(this, p1);
			}
			if(p1.init(this)+actions.get(p1).speed()>=p2.init(this)+actions.get(p2).speed()){
				if(actions.get(p1).usable(this,p2)){
					actions.get(p1).resolve(this,p2);
					checkStamina(p2);
				}			
				if(actions.get(p2).usable(this,p1)){
					actions.get(p2).resolve(this,p1);
					checkStamina(p1);
				}
			}
			else{
				if(actions.get(p2).usable(this,p1)){
					actions.get(p2).resolve(this,p1);
					checkStamina(p1);
				}
				if(actions.get(p1).usable(this,p2)){
					actions.get(p1).resolve(this,p2);
					checkStamina(p2);
				}
			}
			p1.eot(this,p2,actions.get(p2));
			p2.eot(this,p1,actions.get(p1));
			stance.decay();
			stance.checkOngoing(this);

			this.phase=0;
			if(!(p1.human()||p2.human())){
				timer++;
				turn();
			}
			this.setChanged();
			this.notifyObservers();
			
		}
		
	}
	public void automate(){
		while(!(p1.getArousal().isFull()||p2.getArousal().isFull())){
			phase=1;
			p1.regen(true);
			p2.regen(true);
			lowblow.put(p1, false);
			lowblow.put(p2, false);
			actions.put(p1,((NPC)p1).actFast(this));
			actions.put(p2,((NPC)p2).actFast(this));
			clear();
			if(Global.debug){
				System.out.println(p1.name()+" uses "+actions.get(p1).toString());
				System.out.println(p2.name()+" uses "+actions.get(p2).toString());
			}
			if(p1.pet!=null&&p2.pet!=null){
				petbattle(p1.pet,p2.pet);
			}
			else if(p1.pet!=null){
				p1.pet.act(this, p2);
			}
			else if(p2.pet!=null){
				p2.pet.act(this, p1);
			}
			if(p1.init(this)+actions.get(p1).speed()>=p2.init(this)+actions.get(p2).speed()){
				if(actions.get(p1).usable(this,p2)){
					actions.get(p1).resolve(this,p2);
					checkStamina(p2);
				}			
				if(actions.get(p2).usable(this,p1)){
					actions.get(p2).resolve(this,p1);
					checkStamina(p1);
				}
			}
			else{
				if(actions.get(p2).usable(this,p1)){
					actions.get(p2).resolve(this,p1);
					checkStamina(p1);
				}
				if(actions.get(p1).usable(this,p2)){
					actions.get(p1).resolve(this,p2);
					checkStamina(p2);
				}
			}
			p1.eot(this,p2,actions.get(p2));
			p2.eot(this,p1,actions.get(p1));
			stance.decay();
			stance.checkOngoing(this);
			this.phase=0;			
		}
		if(p1.willOrgasm(this)&&p2.willOrgasm(this)){
			state = eval(null);
			p1.evalChallenges(this, null);
			p2.evalChallenges(this, null);
			p2.draw(this,state);
			for(Character viewer: audience){
				if(Global.random(2)==0){
					viewer.watcher(this, p1, p2);
				}else{
					viewer.watcher(this, p2, p1);
				}				
			}
			end();
			return;
		}
		if(p1.willOrgasm(this)){
			state = eval(p2);
			p1.evalChallenges(this, p2);
			p2.evalChallenges(this, p2);
			p2.victory(this,state);
			for(Character viewer: audience){
				viewer.watcher(this, p2, p1);				
			}
			end();
			return;
		}
		if(p2.willOrgasm(this)){
			state = eval(p1);
			p1.evalChallenges(this, p1);
			p2.evalChallenges(this, p1);
			p1.victory(this,state);
			for(Character viewer: audience){
				viewer.watcher(this, p1, p2);				
			}
			end();
			return;
		}
	}
	public void clear(){
		message="";
	}
	public void write(String text){
		message=message+"<br>"+text;
	}
	public void write(Character user, String text){
		if(user.human()){
			message=message+"<br><font color='rgb(100,100,200)'>"+text+"<font color='black'><br>";
		}else{
			message=message+"<br><font color='rgb(200,100,100)'>"+text+"<font color='black'><br>";
		}
	}
	public void report(String text){
		if(!Global.checkFlag(Flag.noReports)){
			message=message+"<font color='rgb(100,100,100)'>"+text+"<font color='black'><br>";
		}
	}
	public String getMessage(){
		return message;		
	}
	public void drop(Character player, Item item){
		used.get(player).add(item);
		player.consume(item, 1);
	}
	public void checkStamina(Character p){
		if(p.getStamina().isEmpty()){
			if(p.has(Trait.planB)&&(p.has(Potion.EnergyDrink)||p.has(Potion.SuperEnergyDrink))){
				if(p.has(Potion.EnergyDrink)){
					p.consume(Potion.EnergyDrink, 1);
				}else{
					p.consume(Potion.SuperEnergyDrink, 1);
				}
				if(p.human()){
					write("Your legs feel weak, but you pop open an energy drink and down it to recover.");
				}
				else{
					write(p.name()+" pulls a hidden energy drink out of her sleeve and chugs it. "
							+ "It seems to be enough to keep her on her feet.");
				}
				p.heal(p.getStamina().max()/20, this);
			}else{
				p.add(new Winded(p));
				if(!stance.prone(p)){
					Character other;
					if(p==p1){
						other=p2;
					}
					else{
						other=p1;
					}
					if((stance.penetration(p)||stance.penetration(other))&&stance.dom(other)){
						if(p.human()){
							write("Your legs give out, but "+other.name()+" holds you up.");
						}
						else{
							write(p.name()+" slumps in your arms, but you support her to keep her from collapsing.");
						}
					}
					else{
						stance = new StandingOver(other,p);
						if(p.human()){
							if(lowblow.get(p)){
								write("You crumple into the fetal position, cradling your sore testicles.");

							}else{
								write("You don't have the strength to stay on your feet. You slump to the floor.");
							}
						}
						else{
							if(lowblow.get(p)){
								if(p.hasBalls()){
									write(p.name()+" falls to the floor with tears in her eyes, clutching her plums.");									
								}else{
									write(p.name()+" falls to the floor with tears in her eyes, holding her sensitive girl parts.");									
								}
							}else{
								write(p.name()+" drops to the floor, exhausted.");
							}
							if(!Global.checkFlag(Flag.exactimages)||p.id()== ID.SOFIA){
								offerImage("Sofia busted.jpg", "Art by AimlessArt");
							}
						}
					}
				}
			}
			
		}
	}
	
	public void next(){
		if(phase==0){
			turn();
		}
		else if(phase==2){
			end();
		}
	}
	public void intervene(Character intruder, Character assist){
		Character target;
		if(p1==assist){
			target=p2;
		}
		else{
			target=p1;
		}
		p1.undress(this);
		p2.undress(this);
		if(target.resist3p(this, intruder, assist)){
			target.gainXP(20+target.lvlBonus(intruder));
			intruder.gainXP(10+intruder.lvlBonus(target));
			intruder.getArousal().empty();
			if(intruder.has(Trait.insatiable)){
				intruder.getArousal().restore((int) (intruder.getArousal().max()*.2));
			}
			intruder.undress(this);
			if(clothespile.contains(intruder.outfit[1].firstElement())){
				target.gain(intruder.getUnderwear());
			}
			intruder.defeated(target);
			intruder.defeated(assist);
		}
		else{
			if(intruder.human()){
				Global.gui().watchCombat(this);
				Global.gui().loadTwoPortraits(p1,p2);
			}else if(p1.human()){
					Global.gui().loadTwoPortraits(p2,intruder);
			}else if(p2.human()){
					Global.gui().loadTwoPortraits(p1,intruder);
			}
			intruder.intervene3p(this, target, assist);
			assist.victory3p(this, target,intruder);
			phase=2;
			if(!(p1.human()||p2.human()||intruder.human())){
				end();
			}
			else if(intruder.human()){
				Global.gui().watchCombat(this);
			}
		}
		this.setChanged();
		this.notifyObservers();		
	}
	public boolean end(){
		clear();
		for(Character p: used.keySet()){
			for(Item i: used.get(p)){
				p.gain(i,1);
			}
		}
		p1.state=State.ready;
		p2.state=State.ready;
		p1.endofbattle(p2, this);
		p2.endofbattle(p1, this);
		location.endEncounter();
		boolean ding=false;
		if(p1.getXP()>=95+(5*p1.getLevel())){
			p1.ding();
			if(p1.human()){
				ding=true;
			}
		}
		if(p2.getXP()>=95 +(5*p2.getLevel())){
			p2.ding();
			if(p2.human()){
				ding=true;
			}
		}
		return ding;
	}
	public void petbattle(Pet one, Pet two){
		int roll1=Global.random(20)+one.power();
		int roll2=Global.random(20)+two.power();
		if(one.gender()==Trait.female&&two.gender()==Trait.male){
			roll1+=3;
		}
		else if(one.gender()==Trait.male&&two.gender()==Trait.female){
			roll2+=3;
		}
		if(roll1>roll2){
			one.vanquish(this,two);
		}
		else if(roll2>roll1){
			two.vanquish(this,one);
		}
		else{
			write(one.own()+one+" and "+two.own()+two+" engage each other for awhile, but neither can gain the upper hand.");
		}
	}
	public Combat clone() throws CloneNotSupportedException {
	     Combat c = (Combat) super.clone();
         c.p1 = (Character) p1.clone();
         c.p2 = (Character) p2.clone();
         c.clothespile = new ArrayList<Clothing>();
         c.stance = (Position) stance.clone();
         c.used = new HashMap<Character, ArrayList<Item>>();
         c.used.put(c.p1, new ArrayList<Item>());
         c.used.put(c.p2, new ArrayList<Item>());
         c.actions = new HashMap<Character, Skill>();
         c.lowblow = new HashMap<Character, Boolean>();
         c.lowblow.put(c.p1, false);
         c.lowblow.put(c.p2, false);
         c.timer = timer;
         c.images = new HashMap<String, String>();
         if (c.stance.top == p1) c.stance.top = c.p1;
         if (c.stance.top == p2) c.stance.top = c.p2;
         if (c.stance.bottom == p1) c.stance.bottom = c.p1;
         if (c.stance.bottom == p2) c.stance.bottom = c.p2;
         return c;
	}
	public Skill lastact(Character user){
		if(user==p1){
			return actions.get(p1);
		}
		else if(user==p2){
			return actions.get(p2);
		}
		else{
			return null;
		}
	}
	public void offerImage(String path, String artist){
		images.put(path, artist);
	}
	public void showImage(){
		String imagePath = "";
		if((p1.human()||p2.human())&&!Global.checkFlag(Flag.noimage)){
			if(!images.isEmpty()){
				imagePath = images.keySet().toArray(new String[images.size()])[Global.random(images.size())];
			}
			if(imagePath!=""){
				Global.gui().displayImage(imagePath,images.get(imagePath));
			}
		}
		images.clear();
	}
	public void clearImages(){
		images.clear();
	}
	public void forfeit(Character player){
		end();
	}
	public void reportPleasure(Character target, int magnitude, Result mod){
		String type = "pleasure";
		if(mod==Result.foreplay){
			type = "foreplay pleasure";
		}else if(mod==Result.finisher){
			type = "finishing pleasure";
		}
		if(target.human()){
			if(target.canReadAdvanced(target)){
				report(String.format("You receive %d %s.", magnitude, type));
			}else if(target.canReadBasic(target)){
				report(String.format("You are aroused by %s %s.", target.getBasicArousalDamage(type, magnitude), type));
			}
		}else if(getOther(target).human()){
			if(getOther(target).canReadAdvanced(target)){
				report(String.format("%s receives %d %s.", target.name(),magnitude, type));
			}else if(getOther(target).canReadBasic(target)){
				report(String.format("%s receives %s %s.", target.name(), target.getBasicArousalDamage(type, magnitude), type));
			}
		}
	}
	public void reportTemptation(Character target, int magnitude, Result mod){
		String type = "temptation";
		if(mod==Result.foreplay){
			type = "foreplay temptation";
		}else if(mod==Result.finisher){
			type = "finishing temptation";
		}
		if(target.human()){
			if(target.canReadAdvanced(target)){
				report(String.format("You are aroused by %d %s.", magnitude, type));
			}else if(target.canReadBasic(target)){
				report(String.format("You are aroused by %s %s.", target.getBasicArousalDamage(type, magnitude), type));
			}
		}else if(getOther(target).human()){
			if(getOther(target).canReadAdvanced(target)){
				report(String.format("%s receives %d %s.", target.name(),magnitude, type));
			}else if(getOther(target).canReadBasic(target)){
				report(String.format("%s receives %s %s.", target.name(), target.getBasicArousalDamage(type, magnitude), type));
			}
		}
	}
	public void reportPain(Character target, int magnitude){
		if(target.human()){
			if(target.canReadAdvanced(target)){
				report(String.format("You take %d pain damage.", magnitude));
			}else if(target.canReadBasic(target)){
				report(String.format("You take %s pain damage.", target.getBasicPain(magnitude)));
			}
		}else if(getOther(target).human()){
			if(getOther(target).canReadAdvanced(target)){
				report(String.format("%s takes %d pain damage.", target.name(),magnitude));
			}else if(getOther(target).canReadBasic(target)){
				report(String.format("%s takes %s pain damage.", target.name(),target.getBasicPain(magnitude)));
			}
		}
	}
	public void reportWeaken(Character target, int magnitude){
		if(target.human()){
			if(target.canReadAdvanced(target)){
				report(String.format("You are weakened by %d damage.", magnitude));
			}else if(target.canReadBasic(target)){
				report(String.format("You are weakened %s.", target.getBasicWeaken(magnitude)));
			}
		}else if(getOther(target).human()){
			if(getOther(target).canReadAdvanced(target)){
				report(String.format("%s is weakened by %d damage.", target.name(),magnitude));
			}else if(getOther(target).canReadBasic(target)){
				report(String.format("%s is weakened %s.", target.name(),target.getBasicWeaken(magnitude)));
			}
		}
	}
	public void reportCalm(Character target, int magnitude){
		if(target.human()){
			if(target.canReadAdvanced(target)){
				report(String.format("You calm down by %d arousal.", magnitude));
			}else if(target.canReadBasic(target)){
				report(String.format("You calm down %d.", target.getBasicCalm(magnitude)));
			}			
		}else if(getOther(target).human()){
			if(getOther(target).canReadAdvanced(target)){
				report(String.format("%s calms down by %d arousal.", target.name(),magnitude));
			}else if(getOther(target).canReadBasic(target)){
				report(String.format("%s calms down %s.", target.name(), target.getBasicCalm(magnitude)));
			}
		}		
	}
	public void reportHeal(Character target, int magnitude){
		if(target.human()){
			if(target.canReadAdvanced(target)){
				report(String.format("You heal %d stamina damage.", magnitude));
			}else if(target.canReadBasic(target)){
				report(String.format("You heal %s.", target.getBasicHeal(magnitude)));
			}			
		}else if(getOther(target).human()){
			if(getOther(target).canReadAdvanced(target)){
				report(String.format("%s heals %d stamina damage.", target.name(),magnitude));
			}else if(getOther(target).canReadBasic(target)){
				report(String.format("%s heals %s.", target.name(), target.getBasicHeal(magnitude)));
			}
		}
	}
	public void reportStatus(Character target, Status sts){
		if(target.human()){
			if(target.canReadBasic(target)){
				report("You gain the status "+sts.toString()+".");
			}			
		}else if(getOther(target).human()){
			if(getOther(target).canReadBasic(target)){
				report(target.name()+" gains the status "+sts.toString()+".");
			}
		}
	}
	public void reportStatusLoss(Character target, Status sts){
		if(target.human()){
			if(target.canReadBasic(target)){
				report("You are no longer affected by the status "+sts.toString()+".");
			}
		}else if(getOther(target).human()){
			if(getOther(target).canReadBasic(target)){
				report(target.name()+" loses the status "+sts.toString()+".");
			}
		}
	}
	public void addWatcher(Character voyeur){
		audience.add(voyeur);
	}
	public void lowBlow(Character target){
		lowblow.put(target, true);
	}
	public boolean getLowBlow(Character target){
		return lowblow.get(target);
	}
	public boolean underwearIntact(Character target){
		Clothing underwear = target.getOutfitItem(ClothingType.UNDERWEAR);
		if(underwear==null){
			return true;
		}else{
			return clothespile.contains(underwear);
		}
	}
	public boolean attackRoll(Skill skill, Character attacker, Character target){
		int roll = Global.random(20)+1;
		int tohit = roll+attacker.tohit()+skill.accuracy();
		if(stance.dom(attacker)){
			tohit *= 1.3f;
		}else if(stance.sub(attacker)){
			tohit *= .8;
		}
		int ac = target.ac();
		if(roll==20){
			return true;
		}else if(tohit==0 || tohit < ac ){
			if(tohit+10 < ac + target.bonusCounter()){
				target.counterattack(attacker,skill.type(), this);
			}
			return false;
		}
		return true;
	}

	public boolean effectRoll(Skill skill, Character attacker, Character target, int power){
		int roll = Global.random(20)+1;
		int tohit = roll+attacker.tohit()+skill.accuracy();
		if(stance.dom(attacker)){
			tohit *= 1.3f;
		}else if(stance.sub(attacker)){
			tohit *= .8;
		}
		int ac = target.ac();
		if(roll==20 ||  tohit > ac){
			return true;
		}else{
			return false;
		}
	}
}
