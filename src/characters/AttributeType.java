package characters;

public enum AttributeType {
    Basic,
    Passive,
    Advanced,
    Specialization
}
