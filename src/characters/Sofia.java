package characters;

import Comments.CommentGroup;
import Comments.CommentSituation;
import actions.Action;
import actions.Movement;
import combat.Combat;
import combat.Result;
import daytime.Daytime;
import global.*;
import items.Clothing;
import items.Toy;
import items.Trophy;
import skills.*;
import status.Stsflag;

import java.util.ArrayList;
import java.util.HashSet;

public class Sofia implements Personality {
    public NPC character;
    public Sofia(){
        character = new NPC("Sofia",ID.SOFIA,1,this);
        resetOutfit();
        character.closet.add(Clothing.bra);
        character.closet.add(Clothing.tanktop);
        character.closet.add(Clothing.panties);
        character.closet.add(Clothing.shorts);
        character.change(Modifier.normal);
        character.mod(Attribute.Power, 2);
        character.mod(Attribute.Speed, 1);
        character.mod(Attribute.Footballer, 2);
        character.getStamina().gain(30);
        Global.gainSkills(character);
        character.add(Trait.female);
        character.add(Trait.wrassler);
        character.add(Trait.achilles);
        character.add(Trait.striker);
        character.setUnderwear(Trophy.SofiaTrophy);
        character.plan = Emotion.hunting;
        character.mood = Emotion.confident;
        character.strategy.put(Emotion.sneaking, 1);
        character.strategy.put(Emotion.bored, 1);
        character.preferredSkills.add(Kick.class);
        character.preferredSkills.add(LowBlow.class);
        character.preferredSkills.add(Fuck.class);
        character.preferredSkills.add(Footjob.class);
        character.preferredSkills.add(OpenGoal.class);
        character.preferredSkills.add(KissBetter.class);
        if(Global.checkFlag(Flag.PlayerButtslut)){
            character.preferredSkills.add(AssFuck.class);
            character.preferredSkills.add(Turnover.class);
            character.preferredSkills.add(Strapon.class);
            character.preferredSkills.add(FingerAss.class);
        }
    }
    @Override
    public Skill act(HashSet<Skill> available, Combat c) {
        HashSet<Skill> mandatory = new HashSet<Skill>();
        HashSet<Skill> tactic = new HashSet<Skill>();
        Skill chosen;
        for(Skill a:available){
            if(character.is(Stsflag.orderedstrip)){
                if(a.toString()=="Undress"||a.toString()=="Strip Tease"){
                    mandatory.add(a);
                }
            }
            if(character.has(Trait.strapped)){
                if((a.toString()=="Mount")){
                    mandatory.add(a);
                }
                if(a.toString()=="Turn Over"){
                    mandatory.add(a);
                }
            }
        }
        if(!mandatory.isEmpty()){
            Skill[] actions = mandatory.toArray(new Skill[tactic.size()]);
            return actions[Global.random(actions.length)];
        }
        ArrayList<HashSet<Skill>> priority = character.parseSkills(available, c);
        if(Global.checkFlag(Flag.hardmode)&& Scheduler.getMatch().condition!=Modifier.quiet){
            chosen = character.prioritizeNew(priority,c);
        }
        else{
            chosen = character.prioritize(priority);
        }
        if(chosen==null){
            tactic=available;
            Skill[] actions = tactic.toArray(new Skill[tactic.size()]);
            return actions[Global.random(actions.length)];
        }
        else{
            return chosen;
        }
    }

    @Override
    public Action move(HashSet<Action> available, HashSet<Movement> radar, Match match) {
        Action proposed = character.parseMoves(available, radar, match);
        return proposed;
    }

    @Override
    public void rest(int time, Daytime day) {
        if(!(character.has(Toy.Crop)||character.has(Toy.Crop2))&&character.money>=200){
            character.gain(Toy.Crop);
            character.money-=200;
        }
        character.visit(4);
        String loc;
        ArrayList<String> available = new ArrayList<String>();
        available.add("XXX Store");
        available.add("Bookstore");
        if(character.rank>0){
            available.add("Workshop");
        }
        available.add("Play Video Games");
        for(int i=0;i<time-5;i++){
            loc = available.get(Global.random(available.size()));
            day.visit(loc, character, Global.random(character.money));
        }
        if(character.getAffection(Global.getPlayer())>0){
            Global.modCounter(Flag.SofiaDWV, 1);
        }
    }

    @Override
    public String bbLiner() {
        switch(Global.random(3)){
            case 2:
                return "<i>\"Oooooh, that must hurt sooo bad!\"</i> Sofia trills holding her mound and leaning in with an excited smirk as you clutch your aching balls.  You're not sure if she's mocking you, or sympathizing.";
            case 1:
                return "<i>\"Awww, do you need a moment to hold them?\"</i> Sofia purrs, standing in front of you with a mocking smirk, her hands on her hips and thighs confidently parted.  <i>\"Maybe wear a cup next time.\"</i>";
            default:
                return "You moan in agony and curl over at the hips, eyes screwed shut as you cup your throbbing balls. You hear a soft, feminine moan, and open your eyes to find Sofia's slender mocha fingers " +
                        "drawing firm circles around her moist sex, her bright eyes locked onto you with unnerving intensity.  <i>\"I don't know what's sexier… crushing your cojones, or watching you clutch them after…,\"</i> she purrs in a husky whisper, taking in your predicament with unconcealed lust.";
        }
    }

    @Override
    public String nakedLiner() {
        return "Sofia turns a little to the side with a coy smirk, reaching down to cup her plump venus mound in both hands. <i>\"Mmmm, you wouldn't hit me down there... would you?\"</i> she purrs, a naughty, hopeful smile tugging at her lips.";
    }

    @Override
    public String stunLiner() {
        return "<i>\"Oooooh! You got me so good!\"</i>";
    }

    @Override
    public String taunt() {
        return "<i>\"We could just skip to the end, where you beg me for mercy, but the journey's my favorite part.\"</i>";
    }

    @Override
    public String victory(Combat c, Result flag) {

        return "Sofia's relentless energy and long, powerful legs are wearing you out – at the same time, her voluptuous curves and lithe figure have gotten you rock hard, and your balls are swollen to bursting.  The two of you grapple for the upper hand, and soon you are both standing face-to-face, wrangling for an edge.  With a seductive glimmer in her eye, Sofia catches you off-guard by leaning in and planting a warm, enticing kiss on your lips.  The kiss deepens and your mouths open, your tongues meet and begin stroking each other eagerly.  You begin to lose yourself in her delicious lips, and you barely notice as she takes your hands in hers and slowly interlocks your fingers with her own.  Keeping a firm grip on both of your hands, she takes a small step backwards and, before you can react, kicks her foot upwards between your legs, smashing her bare toes into your balls.<p>" +
                "The kick wasn't exceptionally hard, but it causes your knees to buckle and your body to lurch forward with the impact of the blow; you manage to stay on your feet, and bring your knees together protectively.  However, Sofia is keeping her foot buried aggressively in your crotch, and your hands trapped tightly in hers – the nut-shot didn't completely incapacitate you, but it did sap the strength you needed to free your hands and fight back.  Sofia has you in a tight spot; you can't reposition yourself without opening your legs, and if you open your legs you will likely just get kicked again. <p>" +
                "Sofia's foot is still trapped between your thighs, planted firmly against your nuts.  You can feel her toes wriggle against your sack, rolling your tender balls around and causing your boner to twitch expectantly.  There is nothing you can do to resist her as her toes skillfully coax a drop of precum from the tip of your erection; and as her tiny feet squirm playfully against the your swollen testicles, you realize that you can't hold back.  Within moments, the pressure builds at the base of your scrotum, and your dick erupts a thick, heavy load all over the instep of Sofia's foot.<p>" +
                "A smug smile spreads across Sofia's face as she finally removes her foot from your groin, and she pulls you close to her for another kiss.  She lets go of your hands, and you use them to explore her body, stroking and squeezing her firm breasts and ass, and gently rubbing her slippery clit between your fingers.  Sofia moans softly as you caress and fondle her most sensitive areas, and she gives you an alluring look before turning around and backing up into you, smooshing her plump ass against your dick and balls. <p>" +
                "You reach around from behind her, with one hand fondling her breasts and another hand working its way down her belly and towards her vulva.  With your first and third fingers, you open her soaking lips, and with your second finger you rhythmically caress her clit, occasionally slipping it inside of her as she squirms in your grasp.  Sofia's gorgeous figure in your arms is getting you aroused again, and your dick stiffens against Sofia's ass, snuggling comfortably between her cheeks.<p>" +
                "Sofia is breathing harder and harder, and your dick is begging for another round; she eagerly complies as you bend her forward onto her hands and knees.  You kneel behind her and insert yourself, and within a few strokes you have her clawing at the ground in front of her in ecstasy.  Her generous ass is bouncing like a rubber ball against your hips, and she climaxes with a scream, grinding herself hungrily against your cock.  Her tremors put you over the edge as well, and the two of you writhe in pleasure as you are wracked by another explosive orgasm, pumping a copious load deep inside of her.<p>" +
                "The two of you shudder for a few moments before you pull out, and you both spend a quiet minute lying next to each other on the ground, catching your breath.  With a playful laugh, Sofia gets up first, and bounds off into the night with a wave.  <i>\"Adios!\"</i>";
    }

    @Override
    public String defeat(Combat c, Result flag) {

        return "After a hard-fought match, you have finally managed to get Sofia on the ropes.  The two of you stand facing each other, locked in a tight grapple.  Sofia is quite strong for her size, and you can't seem to get any leverage to wrestle without opening your legs and leaving your balls vulnerable for one of Sofia's typical nut-shots.  You suddenly get an idea: you feint opening your legs, causing Sofia to predictably jerk her knee upwards into your crotch, but as she does so, you quickly step behind her, wrapping your arms tightly around her torso and pinning her arms to her body. <p>" +
                "Holding her from behind, you move one of your hands down the front of her stomach, making your way to her plump vulva.  <i>\"Aghh!  Once I get free, I'm going to kick you right where it hurts!\"</i> She hisses as she squirms against you, her firm butt grinding enticingly against your groin.  With your pointer and ring finger, you gently part her labia, and with your middle finger you caress Sofia from the base of her vagina to her clit.  <i>\"Ayyy… dios mio!\"</i> Sofia purrs with a shuddering moan.  She is practically dripping wet, and her body squirms against yours with equal parts resistance and ecstasy.  Her clit slowly engorges, and you make tiny circles with your finger around it until it feels like a wet, ripe cherry quivering in your fingers. <p>" +
                "Sofia continues to struggle against your grip; trying to twist herself free and kicking her foot backwards up between your legs, cleanly connecting with both your balls.  Thankfully, there wasn't a lot of power behind the hit, but you impulsively retaliate by seizing Sofia's clit firmly between your thumb and forefinger, and giving it a strong pinch.  Sofia lets out a squeal which is somewhere between a yelp and a moan, and you feel her body soften in your hands, beginning to yield. <i>\"Ohhhh… my pussy!\"</i> Sofia moans heavily, but it's impossible to tell if it's from pain or pleasure.  <p>" +
                "Pressing your advantage, you maintain your hold on her clit, periodically slipping a few fingers inside of her.  Sofia is responding eagerly to your touch, breathing heavily and her knees buckling slightly as her squirms of resistance slowly turn into steady, rhythmic gyrations.  She purrs between heaving breaths, her voice now clearly dripping with desire as her hands wander across your forearms.  You are unable to control yourself from getting rock hard, and as Sofia's moans become increasingly intense, you nestle your hard-on snugly between her generous cheeks.<p>" +
                "Panting and sweating, Sofia looks like she is about to burst – and with a final pinch, you squeeze her clit tightly, bringing her to a powerful, screaming climax.  Sofia convulses in your grip, and goes almost limp as she collapses to the floor, on her hands and knees.  With her marvelous pussy and ass in the air in front of you, your cock practically drags you down on top of her; you position yourself behind her. <i>\"Come on! Please... give it to me!\"</i> she huffs, seeming to get a second wind as she enthusiastically arches her back, spreads her legs, and backs herself onto you. <p>" +
                "Pulling her by the hips, you bounce yourself against Sofia's voluptuous ass, thrusting your cock deeper and deeper into her.  You gasp as Sofia's hand brushes the inside of your thigh, reaching between your legs, freezing mid-thrust.  She wraps her fingers around your dangling jewels, but with tenderness you weren't expecting.  <i>\"Don't stop,\"</i> she whispers with a lustful grin over her shoulder as she gently massages your balls, rubbing and squeezing them sweetly in her soft grasp, gently tugging on your sac to coax the next thrust.  <i>\"I just want to hold your gorgeous balls while you cum. Ohhh yes! Yes, harder!\"</i> she moans as your hips smack against her butt, the soft warmth of her grasp on your plums melding with the moist embrace of her sex around your cock.  Sofia gasps in excited desire as your testicles begin to contract with anticipation against her palm.  With a finishing thrust, you shoot a hot, heavy load inside of Sofia, who squirms and squeals with another orgasm of her own.  <p>" +
                "Sofia flops down onto her back, panting on the floor in front of you, her mocha hand softly cupping her soaked and engorged sex.  <i>\"Thank you,\"</i> she moans between heaving breaths as she leans her head back, shutting her eyes for a moment and giving her sore mound a reassuring squeeze.  It is tempting to join her for a moment of rest, but you instead head back out into the games.<p>";
    }

    @Override
    public String victory3p(Combat c, Character target, Character assist) {
        character.clearGrudge(target);
        character.clearGrudge(assist);
        if(target.human()){
            return "Sofia gently rolls you onto your back, and "+assist.name()+" straddles your face, holding your arms down with her body weight.  " +
                    "Sofia positions herself over your hips and effortlessly slides your cock inside of her; despite her wetness, her tight pussy grips " +
                    "your shaft firmly as she begins to gleefully bounce her generous ass up and down on you.  Unconsciously, your tongue begins to explore " +
                    assist.name()+"'s lips and clit, and she moans softly in approval.  Sofia and "+assist.name()+" are face-to-face as they each ride you, " +
                    "and Sofia brings "+assist.name()+" close to her and kisses her gently while fondling her breasts – "+assist.name()+" responds in kind, " +
                    "rubbing Sofia's slippery clit.  With your nose and mouth buried in "+assist.name()+", and Sofia enthusiastically riding your cock, " +
                    "it doesn't take long for you to explode, shooting an ample load up into Sofia.  A moment after your climax, you feel "+assist.name() +
                    "tense up, squeezing your head between her thighs with a moan, and shuddering with an orgasm of her own.  Almost instantly, Sofia gives " +
                    "in to the work of "+assist.name()+"'s curious fingers, and you feel her womanhood tense and contract around your cock as she climaxes as well. ";
        }else{
            return "You and Sofia have "+target.name()+" right where you want her.  You stand behind "+target.name()+" and bend her slightly forwards; " +
                    "Sofia stands in front of her and captures her with a passionate kiss, interlocking her fingers with "+target.name()+"'s.  You insert " +
                    "yourself into "+target.name()+" from behind, holding onto her hips to stabilize both of you as you begin to gently thrust rhythmically " +
                    "inside of her.  Keeping her fingers interlocked with "+target.name()+"'s, Sofia kneels down in front of her and begins licking and " +
                    "sucking her clit as you penetrate her, and you can feel her tongue also caress the base of your dick and balls.  Before long, " +
                    target.name()+" is wriggling and squirming in ecstasy, and she squeals and convulses in orgasm.  Sofia turns her attention from " +
                    target.name()+"'s clit to your balls, and begins sucking them eagerly – she rolls your balls around inside of her mouth as they lurch " +
                    "and contract, and you can still feel "+target.name()+" constricting around your shaft as you violently erupt inside of her. ";
        }
    }

    @Override
    public String intervene3p(Combat c, Character target, Character assist) {
        if(target.human()){
            return "Your match with "+assist.name()+" has been hard-fought, but you feel like you are running out of steam; you have both stripped each other " +
                    "completely, and you stand face-to-face, grappling desperately for the upper hand.  With a smirk, "+assist.name()+" sweeps her leg out " +
                    "and kicks the inside of your foot, spreading your legs wide open and throwing you off-balance.  However, the kick also left "+assist.name() +
                    " off balance and you are able to knock her down while staying on your feet. You seem to have to advantage, until you feel a foot slam " +
                    "into your dangling ballsack from behind.  As you crumple with a whimper, you catch sight of Sofia standing behind you, looking pleased " +
                    "with herself and savoring the expression on your face.  "+assist.name()+" must have seen her coming and set you up for the perfect kick.  " +
                    "As you hit the ground, the two of them waste no time and pounce on you.";
        }else{
            return "Your match with "+target.name()+" has been uncomfortably close, and you are desperate for an edge.  While locked up in a tight grapple with " +
                    target.name()+", you see Sofia cautiously approaching the two of you; exchanging a knowing glance with Sofia, you realize that you have " +
                    "just found the opportunity you need to win.  Naked and exhausted, you quickly spin "+target.name()+" around so you are grabbing her from " +
                    "behind with her arms pinned at her sides, and you nestle your erection snugly between her butt cheeks as she struggles against your hold.  " +
                    target.name()+" is too distracted to notice Sofia as she darts out in front of the two of you, and kicks her foot swiftly up between both " +
                    "of your legs, simultaneously kicking "+target.name()+" in the pussy with her shin and kicking you in the balls with her foot.  You and " +
                    target.name()+" collapse on top of each other on the ground with a groan, each clutching your pulverized privates, though it seems " +
                    target.name()+" got it far worse than you did.  <i>\"Sorry, "+assist.name()+", I couldn't resist,\"</i> Sofia says, helping you to your feet with a smile " +
                    "and a wink.  <i>\"Now let's finish this, OK?\"</i>";
        }
    }

    @Override
    public String resist3p(Combat c, Character target, Character assist) {
        return null;
    }

    @Override
    public String watched(Combat c, Character target, Character viewer) {
        return "By the time you start watching, "+target.name()+" is already visibly fatigued, while Sofia looks totally fresh. Maybe "+target.name()+" was coming from a long fight already, but more likely it's Sofia's Soccer stamina at play. Sofia literally runs circles around "+target.name()+", grabbing her from behind and putting her into a full nelson. She roughly inserts her knee between "+target.name()+"'s legs and starts vibrating her leg. <p>" +
                "Sofia pushes her leg up as "+target.name()+" struggles against it, her clit mashing violently onto the athlete's knee. Sofia grins as "+target.name()+" practically does all the work for her. Eventually her struggles cease as assault and previous fatigue force her back to arch and a violent orgasm escapes her. <p>" +
                "Sofia loosens her grip, shifting her hands from under "+target.name()+"'s shoulders to her breasts. Sofia licks at her neck, nibbling and sucking on her skin to leave a small hickey as she sags onto the Argentinian's body. <p>" +
                "<i>\"Aw, come on chica,\"</i> Sofia kneads "+target.name()+"'s breasts, eliciting pleasured moans, <i>\"You must have more energy than that. At least enough to get me off.\"</i> <p>" +
                ""+target.name()+" growls and turns around, pushing Sofia against a wall. Sofia grins and pulls "+target.name()+" into a heavy kiss. Their bodies push against each other, legs in the others crotch to push. You watch in fascination as Sofia and "+target.name()+" vie for dominance. Their thighs rub against the other's clits, their hands grope whatever pound of flesh they can hold, and their tongues tangle together. <p>" +
                "With the way "+target.name()+" squirms, you can tell she's cumming again. The shock of her orgasm causes her to reflexively jerk her thigh up into Sofia's pussy. Sofia's eyes go wide and you're sure she just followed suit. <p>" +
                "The two collapse together in a pile of naked afterglow. Sofia whispers something you don't catch into her opponent's ear. Whatever it is sets them both giggling as Sofia starts gathering up their discarded clothes.<p>";
    }

    @Override
    public String describe() {
        return "Sofia looks like a natural athlete. It's in everything about her, from her thick, steely thighs, to the smooth, kinetic quality of her every movement. Her figure is lithe yet voluptuous, with slender arms and a pinched midriff that offsets her prominent D-cup breasts, wide hips, and full bubble-butt. Her wavy, dirty blonde locks strike quite the contrast against her soft, glistening mocha skin, and her large, cheerful brown eyes match her bouncy, yet fiery personality.";
    }

    @Override
    public String draw(Combat c, Result flag) {

        return "You aren't going to make it. Physical exertion aside, Sofia's rough and passionate handling of your body was making dodging difficult as your hard-on slaps against your thighs. Any bit of stimulation at this point was likely to set you off. You barely put any thought to dodging as you start to focus more on how full and thick Sofia's thighs are as she weakly kicks at you, how soft her hands are as they miss a grab at your dick, how frustrated her moans are? <p>" +
                "Feeling that last one was off, you glance up to find Sofia staring right at your dick. She's so distracted that you're able to nick her with a punch, surprising you with a moan in pleasure. Seeing victory and desperately wanting to get off you try to take advantage of Sofia's masochism and go for a right kick between the legs. Sofia, however, catches her footing and lets out her own desperate right kick. <p>" +
                "Time seems to slow down as you watch your legs skim past each other. While you're not sure what your expression was you can see Sofia's face morph into shock, acceptance, then bliss as your foot connects to her hip. A flood of her love juices hits your foot just as your world explodes into white. <p>" +
                "The first thing you notice when you regain consciousness is that your dick is being heavily abused. While that's always a concern with Sofia, thankfully it was the rough pleasurable kind as you open your eyes and see richly colored mocha breasts bouncing quickly in and out of view. Her eyes are glazed and her tongue wages out her mouth as she beats down on your hips like they owed her money. The empty painful feeling in your balls tells you this was a draw but Sofia rides your dick in an attempt to milk cum that wasn't there. Still, you're able to move and you were robbed of an orgasm so you push through the pain and pull yourself over to her to match her pace. <p>" +
                "<i>\"Eres maravilloso!\"</i> She screams out as you pump your hips back, <i>\"Donde has estado toda mi vida!?\"</i> <p>" +
                "Before you could guess what she was saying she locks lips with you. Her hair tickles your skin as her frame bounces heavily onto your chest, her tits rub circles onto your chest and own nipples. Athletic tightness squeezes at your dick, quickly trying to squeeze out rapidly producing cum from you as her muscled arms squeeze you closer to her. <p>" +
                "<i>\"Dentro por favor!\"</i> She barely manages to scream out the words as she was reaching her peak. Her moans intensify as you grab onto her ass. Her powerful legs holding steadfast onto your back, you don't hold back as you roughly lift her up and down your dick. With one final thrust you pull her deep and cum deep inside her, a painful moan escaping your lips as Sofia rests her weight downwards, her firm ass steadily pushing down onto your balls to help push out what cum was building up as she cums violently herself. <p>" +
                "The two of you clamp up against each other. Her sweaty skin and fruity shampoo barely keep you grounded, Sofia herself shivering and screaming next to your ear. Once the last splurt <p>" +
                "leaves you, she gently rides you as she peppers your neck with kisses, trying to coax out one more batch of cum. <p>" +
                "Eventually she stops altogether. Her breath slowly evening out, you wonder if you somehow were too rough on her. Then she slowly raises her hips and disconnects off you with a pop. You watch mesmerized as she leans her head down to your softening dick. A hand brushes through her hair to reveal her smiling face before she begins to lick off the rest of your cum. <p>" +
                "<i>\"So strong and healthy,\"</i> She licks at your shaft as her hands push at your legs to allow her more room. The gentle sensations were tingling your senses, not enough to arouse but certainly enough to catch your attention. Once your dick was clean she turns her whole body around and leaned her head back onto your crotch. An oof escapes you but otherwise the only feeling there was of her soft hair and some pressure, <i>\"Your legs are so strong. And your dick is so durable.\"</i> <p>" +
                "Both a sense of pride and dread fills you as you process her words. Still you grin at the complement, saying you're not that strong, <i>\"No, you are. The only one who wants to kick me that hard to get me off is Jewel. Esa chabona es re copada pero, er, but you're the only guy I know willing to give and take as much as me.\"</i> <p>" +
                "You shrug, absentmindedly playing with her hair and messaging her head. Rules made it so she had to get you off to win. If she just kicked you in the nuts and left you don't think you'd have that good of an impression. She laughs at that. <p>" +
                "<i>\"Fair enough. Still, most boys get scared when I bring my little fetish into the bedroom. Too afraid to hurt me or too scared to get hit. But you? You didn't have to play it my way but you did. That means a lot.\"</i> <p>" +
                "You feel a bit flushed as she gratefully smiles at you. She laughs once more and rolls up to her feet in a show of fitness. She slaps at her ass, giggling as some of your cum slips down her leg, before swinging her it side to side as she leaves. <p>" +
                "<i>\"Rest up amigo. If you want your balls safe you're going to have to up your game.\"</i> <p>" +
                "She licks her lips as she walks out, confidence radiating off each step. Damn that's a nice ass. But is it enough to risk your balls constantly? Yes. No. Maybe. Hm... <p>";
    }

    @Override
    public boolean fightFlight(Character opponent) {
        return fit();
    }

    @Override
    public boolean attack(Character opponent) {
        return true;
    }

    @Override
    public void ding() {
        character.mod(Attribute.Footballer, 1);
        int rand;
        character.getStamina().gain(6);
        character.getArousal().gain(4);
        character.getMojo().gain(2);
        for(int i=0; i<(Global.random(3)/2)+1;i++){
            rand=Global.random(3);
            if(rand==0){
                character.mod(Attribute.Power, 1);
            }
            else if(rand==1){
                character.mod(Attribute.Seduction, 1);
            }
            else if(rand==2){
                character.mod(Attribute.Cunning, 1);
            }
        }
    }

    @Override
    public String startBattle(Character opponent) {
        if(character.getGrudge()!=null) {
            switch (character.getGrudge()) {
                case defensivemeasures:
                    return "Sofia struts up with her usual confidence and intensity, but her stride's... off. Her hips aren't quite swaying as much as usual, and " +
                            "she seems just a bit bow-legged. She stands across from you, confidently parting her thighs with her hands on her hips, her mound " +
                            "seems even a little more pronounced than usual. She smirks, following your gaze, and the sound of hollow plastic as she raps her " +
                            "knuckles against her groin confirms your suspicions. <i>\"Let's see what you can do with my weakness covered up. Getting hit down there " +
                            "makes me hot, but tonight I'm here to beat you!\"</i>";
                case sadisticmood:
                    return "Sofia smirks as she squares up against you, sensually licking her lips as she eyes you up and down, her eyes lingering on your groin. " +
                            "Knowing her proclivities, the intensity of her stare is making you feel pretty uncomfortable. <i>\"The only thing " +
                            "that turns me on more than a big, strong man rolling on the ground and holding his throbbing balls, is you,\"</i> she whispers, " +
                            "<i>\"Rolling on the ground and holding your throbbing balls.\"</i>";
                case cheapshot:
                    return "Sofia's eyes are filled with lust, and as she makes a beeline for you she seems absolutely ready to jump your bones on the spot. You get " +
                            "ready to fight as she closes the distance, but lose your train of thought as she yanks her top off mid-stride. Your eyes rest on her full, " +
                            "heaving bust as it seemingly tries to burst out of her sports bra. She grasps your shoulders and leans in for a kiss, but just as her lips " +
                            "brush yours, your testicles explode into agony! Sofia smirks as you totter backwards, hunched over and cupping your aching balls. <i>\"What, " +
                            "aren't we fighting?\"</i> she coos with a sly grin.";
                default:
                    break;
            }
        }if(opponent.nude()){
            return "Sofia eyes you with a hungry smile. <i>\"Are you trying to tempt me? How could I resist such a perfect package?\"</i>";
        }if(character.nude()){
            return "Sofia runs her hands down her toned body and makes a show of spreading her pussy lips. <i>\"Do you like what you see, baby? The goal is open and waiting for your shot.\"</i>";
        }
        return "Sofia stretches her legs as you approach, drawing your attention to her muscular thighs and perfect ass. <i>\"Are you a fan of football? Time for a little one-on-one.\"</i>";
    }

    @Override
    public boolean fit() {
        return true;
    }

    @Override
    public String night() {
        return null;
    }

    @Override
    public void advance(int Rank) {

    }

    @Override
    public NPC getCharacter() {
        return character;
    }
    @Override
    public boolean checkMood(Emotion mood, int value) {
        switch(mood){
            case angry: case dominant:
                return value>=30;
            case nervous:
                return value>=80;
            default:
                return value>=50;
        }
    }
    @Override
    public float moodWeight(Emotion mood) {
        switch(mood){
            case angry: case dominant:
                return 1.2f;
            case nervous:
                return .7f;
            default:
                return 1f;
        }
    }
    @Override
    public String image() {
        return "assets/sofia_"+ character.mood.name()+".jpg";
    }

    @Override
    public void pickFeat() {
        ArrayList<Trait> available = new ArrayList<Trait>();
        for(Trait feat: Global.getFeats()){
            if(!character.has(feat)&&feat.req(character)){
                available.add(feat);
            }
        }
        if(available.isEmpty()){
            return;
        }
        character.add((Trait) available.toArray()[Global.random(available.size())]);
    }

    @Override
    public CommentGroup getComments() {
        CommentGroup comments = new CommentGroup();
        comments.put(CommentSituation.VAG_DOM_CATCH_WIN, "<i>\"Come on, sexy! Pump me full of your delicious cream!\"</i>");
        comments.put(CommentSituation.VAG_DOM_CATCH_LOSE, "<i>\"Ohhhh, dios mio! I'm too close!\"</i>");
        comments.put(CommentSituation.VAG_SUB_CATCH_WIN, "<i>\"Oh, yes! Harder! Smack my ass with those heavy balls!\"</i>");
        comments.put(CommentSituation.VAG_SUB_CATCH_LOSE, "<i>\"Mmmmm, yes, fill me up!\"</i>");
        comments.put(CommentSituation.BEHIND_DOM_WIN, "<i>\"Now let's have some fun!\"</i>");
        comments.put(CommentSituation.BEHIND_DOM_LOSE, "<i>\"Quit squirming and gimme a clear shot!\"</i>");
        comments.put(CommentSituation.MOUNT_DOM_WIN, "<i>\"Do you want ecstasy, or agony?\"</i> Sofia purrs, pressing her knee gently, but threateningly against your jewels.");
        comments.put(CommentSituation.OTHER_CHARMED, "<i>\"Mmmmm, spread your legs, baby. Show me that big package! I promise I'll be gentle...\"</i>");
        comments.put(CommentSituation.OTHER_ENTHRALLED, "<i>\"Want a taste?\"</i> Sofia purrs in a husky whisper, a  finger softly tracing her plump lovebulge.");
        comments.put(CommentSituation.SELF_HORNY, "<i>\"You're making me so HOT!\"</i>");
        return comments;
    }

    @Override
    public int getCostumeSet() {
        return 1;
    }

    @Override
    public void declareGrudge(Character opponent, Combat c) {
        switch(Global.random(3)){
            case 0:
                character.addGrudge(opponent,Trait.sadisticmood);
                break;
            case 1:
                character.addGrudge(opponent,Trait.cheapshot);
                break;
            default:
                character.addGrudge(opponent,Trait.defensivemeasures);
                break;
        }
    }

    @Override
    public void resetOutfit() {
        character.outfit[0].clear();
        character.outfit[0].add(Clothing.bra);
        character.outfit[0].add(Clothing.tanktop);
        character.outfit[1].clear();
        character.outfit[1].add(Clothing.panties);
        character.outfit[1].add(Clothing.shorts);
    }
}
