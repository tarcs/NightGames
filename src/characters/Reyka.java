package characters;

import daytime.Daytime;
import global.*;

import items.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import Comments.CommentGroup;
import Comments.CommentSituation;
import skills.*;
import actions.Action;
import actions.Movement;
import characters.Attribute;
import combat.Combat;
import combat.Result;
import status.Stsflag;

public class Reyka implements Personality {
	private static final long serialVersionUID = 8553663088141308399L;
	public NPC character;

	public Reyka() {
		this.character = new NPC("Reyka",ID.REYKA, 10, this);
		this.character.outfit[0].add(Clothing.lbustier);
		this.character.outfit[1].add(Clothing.lminiskirt);
		character.closet.add(Clothing.lbustier);
		character.closet.add(Clothing.lminiskirt);
		this.character.change(Modifier.normal);
		this.character.mod(Attribute.Dark, 12);
		this.character.mod(Attribute.Seduction, 12);
		this.character.mod(Attribute.Cunning, 2);
		this.character.mod(Attribute.Speed, 2);
		this.character.setUnderwear(Trophy.ReykaTrophy);
		this.character.getStamina().gain(15);
		this.character.getArousal().gain(60);
		this.character.getMojo().gain(45);
		character.add(Trait.female);
		character.add(Trait.succubus);
		character.add(Trait.darkpromises);
		character.add(Trait.greatkiss);
		character.add(Trait.tailed);
		character.add(Trait.Confident);
		Global.gainSkills(this.character);
		character.plan = Emotion.hunting;
		character.mood = Emotion.confident;
		character.strategy.put(Emotion.hunting, 5);
		character.strategy.put(Emotion.sneaking, 2);
		character.preferredSkills.add(Fuck.class);
		character.preferredSkills.add(Whisper.class);
		character.preferredSkills.add(Fuck.class);
		character.preferredSkills.add(SpawnImp.class);
		character.preferredSkills.add(LustAura.class);
		if(Global.checkFlag(Flag.PlayerButtslut)){
			character.preferredSkills.add(AssFuck.class);
			character.preferredSkills.add(Strapon.class);
			character.preferredSkills.add(TailPeg.class);
			character.preferredSkills.add(FingerAss.class);
		}

	}

	@Override
	public Skill act(HashSet<Skill> available, Combat c) {
		HashSet<Skill> mandatory = new HashSet<Skill>();
		HashSet<Skill> tactic = new HashSet<Skill>();	
		Skill chosen;
		for(Skill a:available){
			if(a.toString()=="Command"||a.toString().equalsIgnoreCase("Fuck")||a.toString().equalsIgnoreCase("Piston")||a.toString().equalsIgnoreCase("Tighten")||a.toString().equalsIgnoreCase("Ass Fuck")){
				mandatory.add(a);
			}
            if(character.is(Stsflag.orderedstrip)){
                if(a.toString()=="Undress"||a.toString()=="Strip Tease"){
                    mandatory.add(a);
                }
            }
			if(character.has(Trait.strapped)){
				if((a.toString()=="Mount")){
					mandatory.add(a);
				}
				if(a.toString()=="Turn Over"){
					mandatory.add(a);
				}
			}
		}
		if(!mandatory.isEmpty()){
			Skill[] actions = mandatory.toArray(new Skill[tactic.size()]);
			return actions[Global.random(actions.length)];
		}
		ArrayList<HashSet<Skill>> priority = character.parseSkills(available, c);
		if(Global.checkFlag(Flag.hardmode)&&Scheduler.getMatch().condition!=Modifier.quiet){
			chosen = character.prioritizeNew(priority,c);
		}
		else{
			chosen = character.prioritize(priority);
		}
		if(chosen==null){
			tactic=available;
			Skill[] actions = tactic.toArray(new Skill[tactic.size()]);
			return actions[Global.random(actions.length)];
		}
		else{
			return chosen;
		}	
	}

	@Override
	public Action move(HashSet<Action> available, HashSet<Movement> radar, Match match) {
		Action proposed = character.parseMoves(available, radar, match);
		return proposed;
	}

	@Override
	public void rest(int time, Daytime day) {
		if(!(character.has(Toy.Strapon)||character.has(Toy.Strapon2)) && Global.checkFlag(Flag.PlayerButtslut)
				&& character.money>=200 && character.getPure(Attribute.Seduction)>=5){
			character.gain(Toy.Strapon);
			character.money-=200;
		}
		if(!(character.has(Toy.Dildo)||character.has(Toy.Dildo2))&&character.money>=250){
			character.gain(Toy.Dildo);
			character.money-=250;
		}
		if(!(character.has(Toy.Tickler)||character.has(Toy.Tickler2))&&character.money>=300){
			character.gain(Toy.Tickler);
			character.money-=300;
		}
		if(!(character.has(Toy.Strapon)||character.has(Toy.Strapon2))&&character.money>=600&&character.getPure(Attribute.Seduction)>=20){
			character.gain(Toy.Strapon);
			character.money-=600;
		}
		if(!(character.has(Toy.Strapon)||character.has(Toy.Strapon2)) && Global.checkFlag(Flag.PlayerButtslut)
				&& character.money>=200 && character.getPure(Attribute.Seduction)>=5){
			character.gain(Toy.Strapon);
			character.money-=200;
		}
		String loc;
		ArrayList<String> available = new ArrayList<String>();
		available.add("Black Market");
		available.add("XXX Store");
		available.add("Workshop");
		available.add("Play Video Games");
		for(int i=0;i<time;i++){
			loc = available.get(Global.random(available.size()));
			day.visit(loc, character, Global.random(character.money));
		}
		character.visit(3);
		if(Global.random(3)>1){
			character.gain(Component.Semen);
		}
		if(character.getAffection(Global.getPlayer())>0){
			Global.modCounter(Flag.ReykaDWV, 1);
		}
	}

	@Override
	public String bbLiner() {
		switch(Global.random(2)){
		case 1:
			return "<i>\"That wasn't too hard, was it?  We better make sure everything still works properly!\"</i>";
		default:
			return "Reyka looks at you with a pang of regret: <i>\"In hindsight, damaging"
				+ " the source of my meal might not have been the best idea...\"</i>";
		}
	}

	@Override
	public String nakedLiner() {
		return "<i>\"You could have just asked, you know.\"</i> As you gaze upon her naked form,"
				+ " noticing the radiant ruby adorning her bellybutton, you feel"
				+ " sorely tempted to just give in to your desires. The hungry look"
				+ " on her face as she licks her lips, though, quickly dissuades you"
				+ " from doing so";
	}

	@Override
	public String stunLiner() {
		return "Reyka is laying on the floor, her wings spread out behind her,"
				+ " panting for breath";
	}

	@Override
	public String taunt() {
		return "<i>\"You look like you will taste nice. Maybe if let me have "
				+ "a taste, I will be nice to you too\"</i>";
	}

	@Override
	public String victory(Combat c,Result flag) {
		Character opponent=c.getOther(character);
		character.clearGrudge(opponent);
		if (opponent.hasDick())
			character.gain(Component.Semen);
		character.arousal.empty();
		if(flag==Result.anal){
			if(Global.getValue(Flag.PlayerAssLosses) == 0){
				Global.flag(Flag.tookAssReyka);
			}
			Global.modCounter(Flag.PlayerAssLosses, 1);
			return "Reyka alternates between long hard thrusts and sensual grinding to keep you from getting used to the stimulation, and the pleasure it is " +
			"inflicting on you stops you from mustering the resolve to fight back. <i>\"I do love a good bit of pegging.\"</i> Reyka comments as she begins " +
			"to gently rock the head of the strapon over your prostate, leaving you breathing hard as your mouth hangs open. <p>"
			+ "<i>\"There's a special " +
			"pleasure in making a boy a little butt slave.\"</i> Her words shock you and cause your resistance to slip a little. <i>\"Hmmm?\"</i> She purrs <i>\"Would " +
			"you like that?\"</i> she asks, picking up the pace of her thrusting. <i>\"To be my little pet boy slut?\"</i> Your only response is to cum. Hard. Ropes " +
			"of cum fall to the ground below you.<p>"
			+ "Reyka pouts as she pulls out <i>\"Such a good waste of semen though.\"</i> she tuts. <i>\"Looks like you " +
			"still owe me a meal.\"</i> She smirks in a way that makes your eyes flash quickly left to right, looking for an escape route. Reyka is too quick " +
			"however and soon you find yourself pinned with your still hard cock buried deep in her pussy.<p>"
			+ "She rides you until you cum again and she " +
			"has cum twice herself. She stands up and begins collecting her clothes and her spoils as the victor.<p>"
			+ "She turns to you. <i>\"The offer still " +
			"stands; you'd make a great sub if you're ever interesting in broadening your sexual horizons. Open minded men are hard to find.\"</i> She admits, " +
			"smiling.<p>"
			+ "You shake your head; you don't think that sort of thing would really suit you. Her smile deflates some but she nods her head and " +
			"turns to go.<p>"
			+ "<i>\"Let me know if that ever changes, I'd definitely enjoy opening your mind,\"</i> she calls over her shoulder as she leaves.";
		}
		if(opponent.is(Stsflag.enthralled)){
			return "With your mind completely ensnared by Reyka's command, you don't even attempt to hold on as she stimulates you to the very limit of your arousal, just before you finish however, the succubus speaks<p>" +
					"<i>\"Don't cum yet\"</i> Reyka orders firmly, and under her spell you suddenly find yourself unable not to comply. Though your member yearns for release Reyka's command keeps you from cumming, knowing she has you exactly where she wants you she gives you a (literally) devilish smile before diving on top of you, mounting your chest and facing your groin. <p>" +
					"You can feel Reyka begin to absentmindedly play with your boner before she repeats herself.<p>" +
					"<i>\"Do not cum until I say so\"</i> she speaks commandingly before her tone softens <i>\"You have no idea how long I've waited to have you like this "+opponent.name()+"\"</i> Her voice, still laced with dark magic somehow floods you with arousal even further, leaving your cock to tremble at her every motion. Then she leans forwards and takes you in her mouth. Immediately you're flooded with the sensation of her mouth expertly teasing your full length, her tongue salaciously lapping at the head. Within seconds your body begins practically aching for release, your member spasms desperately at every touch of Reyka's warm, wet tongue.<p>" +
					"Reyka however shows no sign of letting you finish however, even though she can clearly tell how desperate you're becoming. She redoubles her efforts as you buck your hips futilely, the need to finish becoming almost too much to bare, as you writhe underneath her submissively, she suddenly pulls away and sits back up. <p>" +
					"<i>\"Alright "+opponent.name()+" this is starting to feel maybe a little too cruel, so, when I kiss the tip, you can cum okay?\"</i> She says with a surprisingly affectionate tone, turning her head to flash you a sweet sideways smile as she begins playing with you absentmindedly with one hand. She watches you writhe and moan with an amused expression until she decides she's toyed with you enough. She turns back around, and still tenderly stroking your shaft, leans over your crotch once more. After a couple of agonisingly long seconds, you feel Reyka peck the head of your dick with her soft gentle lips and the mind control breaks and you're thrown into what's possibly the most intense orgasm of your life! As you cum Reyka wraps her lips around your dick and hungrily swallows your multiple loads until finally you finish, lying dazed, completely spent.  As your mind swims back into clarity, you hear Reyka speak softly<p>" +
					"<i>\"Now that was a meal, I'll be sure to come for you next time I need a mid game snack...\"</i>";
		}
		if(character.pet!=null && character.has(Trait.royalguard)){
			return "Reyka's sensual charms are difficult enough to combat when she's alone, but the added pressure her imp provides has left you struggling to keep up.  Your raging hard-on can barely resist Reyka's shapely form as it bounces tantalizingly in front of you, and her imp weaves between your legs, jerking and sucking you off the moment your defense lapses.  With a drop of precum beginning to form at the tip of your dick, you realize you are quickly running out of options to turn this match around.<p>" +
					"Suddenly, an opportunity presents itself – Reyka lunges for you, but you manage to sidestep her advance, and you quickly grab her from behind, with your arms wrapped around her waist.  By wedging your legs in between hers, you wrestle her to the ground in front of you, forcing her onto her hands and knees.  Wasting no time, you line yourself up behind her, and insert your throbbing member; Reyka's pussy is so thoroughly engorged and lubricated that it practically radiates heat, and your cock slides in its full depth with no resistance.  With both of you on your knees, you widen your stance, opening Reyka's legs further and giving you more thrusting power.  Reyka shudders at your slow, deep thrust, and it seems you might just turn this match around.<p>" +
					"You are fully inside of Reyka when your rally is rudely interrupted by a small hoof swinging up between your legs, kicking both of your balls upwards and painfully rebounding them off of Reyka's clit.  You had momentarily forgotten about Reyka's imp, and now your poor battered balls are paying the price as your whole body instinctively curls up, and the next thing you know you are lying on the ground in the fetal position, your hands cradling your aching gonads.<p>" +
					"Reyka stands up and surveys the situation, and a small scowl forms on her face.  She slowly turns to face the imp, who wears a look of both shame and dread as she sees Reyka's disappointment.  Reyka's eyes narrow slightly as she asks the imp, <i>\"Young lady, did you hit [player name] in the balls?\"</i>  From your position on the floor, you see the imp nod sheepishly, avoiding eye contact.  Reyka crouches down next to you, and gently rolls you onto your back.  You are in too much pain to resist as she slowly coaxes her hand underneath yours to gently cup your balls.  Her soft fondling takes some of the pain away, and as her other hand grips your shaft, you realize you are still rock hard.  Not only that, but you feel a sizable load begin to build inside of you, aching to be released.<p>" +
					"Reyka continues massaging your tender testicles and stroking you up and down the length of your shaft as she addresses the imp, <i>\"Do you know how delicate these little things are?\"</i>  Again, the imp nods, looking away. Reyka goes on, <i>\"And do you know how easy it would be to damage them?  These poor, vulnerable little balls of his...\"</i> Reyka seems almost transfixed by her handiwork, she can obviously feel your dick begin to twitch in her grip and your balls begin to tighten in anticipation of a massive orgasm.  She lowers her head towards your erection, her mouth widening as she gets closer and closer, her tongue hanging out of her mouth as she lowers it onto your tip, and as she makes a slow, wet circle on your glans with her tongue, your hips involuntarily buck forward, practically begging for relief from the pressure building in your glands. <p>" +
					"Reyka takes you deep inside of her mouth, taking her time to suck you from the base of your shaft to the tip, and all the while still protectively cradling your swollen balls in her hand.  You see relief on the imp's face  - it's punishment seemingly forgotten, and you see it slink, disappearing into the shadows as you lose track of it, instead focusing on the amazing oral service Reyka is giving you.  Beneath her masterful touch, it doesn't take long for the pleasure Reyka is giving you to overcome the lingering pain in your balls, and when you explode inside of Reyka's mouth; she hungrily swallows every last drop. <p>" +
					"Completely spent, bruised, drained and satisfied, your body goes limp as you lay on the floor.  Reyka licks the last remaining drops from her lips and, still gently fondling your balls, gives you a long, deep kiss.  She stands up, looking sated, and composes herself before quietly exiting back out into the night.  You take a minute to yourself on the floor, staring upwards, before you can find the strength to get up and return to the games.<p>";
		}
		if (flag == Result.intercourse){
			return "When it becomes obvious that she's going to win, Reyka takes control. Her tail lashes around your neck, just hard enough to "
					+ "hint that things could get worse for you if you don't behave, and she makes sure she's on top.<p>"
					+ "Soon, you have no say in this anymore. She's got you pinned with your hands against the ground, dizzy and thoughtless "
					+ "from the sight and smell and feel of her. Your cock feels like she's got it in a velvet fist, too hot by half, and "
					+ "she expertly shifts her hips against you to milk a load out of your balls.<p>"
					+ "All that time, she keeps eye contact with you. You can't seem to so much as blink. Your orgasm doesn't exactly seem to "
					+ "end, either, but just... elongate, into something else. Some greater, extended experience, something tantric you can't "
					+ "identify.<p>"
					+ "Reyka leans down to run her tongue along your lips, laughing, and even these fractured, scattered thoughts you're "
					+ "having become impossible--<p>"
					+ "<i>Your life begins and ends with this bed.<p>"
					+ "You don't have to leave. You don't know why you'd want to. Sometimes, in memories that seem veiled by smoke, you remember "
					+ "things like having to eat, or mortal ambitions you once had. Those are gone almost as soon as they occur, washed away on "
					+ "slow waves of insatiable lust. Your universe is six feet across and twelve feet long, covered in silk sheets.<p>"
					+ "Your mistress needs you, and you obey. She sinks down onto you, her hands flat against your naked chest, and rides you "
					+ "at her own pace.<p>"
					+ "You know what she wants without having to ask. It's what you want. You'd jump into a fire for the mistress, kill a man, "
					+ "change the course of history. You move as she prefers, as little or as much as she demands, the two of you together in a "
					+ "silent symphony.<p>"
					+ "When you come, you give her much more than simple semen. Heat. Strength. Life. Every time you convulse and fill her with "
					+ "another gush of come, you send another few trailing days of your existence into her and are glad for the contribution.<p>"
					+ "One day, she may take the last one, and you'll die with a smile on your face--</i><p>"
					+ "You wake up.<p>"
					+ "You're naked and alone, left in a corner of the room where you won't be noticed immediately by anyone who walks in. "
					+ "Your clothes are, of course, nowhere to be found. A nearby wall clock tells you that you can't have been unconscious "
					+ "for more than a couple of minutes.<p>"
					+ "It feels like you just donated blood. It takes you three tries to stand, but once you do, it's easier than you "
					+ "thought it'd be. After a couple of minutes, you manage to take one step, then another, and you feel your strength "
					+ "slowly coming back. You're not out of the Games just yet.<p>"
					+ "That wasn't a dream, you think. That was proof of intention. You've been given a sneak preview, somehow, of life as "
					+ "Reyka's pet, in her larder, should she make good on those threats of hers.<p>"
					+ "Now that you're awake, it isn't all that enticing... except in a distant, what-if sort of way. You contemplate what "
					+ "it'd be like to live like that, free from all concerns. The ultimate submissive. Nothing for you but what she wants.<p>"
					+ "You shake your head and go back out into the fray.";
		} else if(character.has(Trait.limitedpotential)){
		    return "It was a close fight. You were grasping Reyka from behind, your hands wrapped around her breast and clit in a desperate attempt to get her off before her combined tail and thighjob could finish you off. Despite her natural perfume clouding your mind, you desperately keep her body tight against yours. If you give her any space to move she'd push your dick inside her and easily secure the win. As agonizing as it is, you hold on and continue caressing her body in the vain hope she'd cum first. <p>" +
                    "She doesn't. It proves too much and you could feel your cum blasting out between her thighs. A startled yelp comes out of Reyka before her hands quickly reach down around your other head. As your cum seeps through her fingers, she slowly pulled them back up to her face in surprise. <p>" +
                    "Being right behind her, you get a front row seat to Reyka licking every bit of spunk with her long tongue. She can tell your watching as her tongue plays with her hand, switching between slow and quick laps up and down her hand inch by inch, making sure to double back if there's even a hint of white left on her hand. Once the last finger is wiped clean, she takes a long gulp and sighs in pleasure. <p>" +
                    "She suddenly smiles, content to use your back as a rest as she giggles. You stare at her, wondering if holding her in your arms is a good idea right now. You try to pull the hand on her breast off only to find her suddenly gripping it and placing it right back. She coos as she feels your touch. <p>" +
                    "<i>\"No no, none of that. You need to be taught your place.\"</i> <p>" +
                    "You remember all the times you've fought against her and point out that she does. Rather often even when she cums first. <p>" +
                    "<i>\"Well, I mean, you and the girls have been getting a bit too complacent,\"</i> There's a slight tint of red on her face as she corrects herself, <i>\"You all need to know what the proper relationship between humans and demons are. I might as well start with you.\"</i> <p>" +
                    "Suddenly the demonic energy that was missing in the last few seconds skyrockets around her. <p>" +
                    "<i>\""+opponent.name()+",\"</i> Her voice turns sultry as she looks up at you, <i>\"Do you know how many times people have fallen to my allure?\"</i> <p>" +
                    "Despite your dick hardening between her legs, you can still think clearly enough to answer. You'd say she's succeeded every time. <p>" +
                    "<i>\"Hm, somewhat. I've never had trouble dragging any human into a sex-crazed haze before. Now? I can barely make a bunch of horny college students cum. I can't help but feel you personally responsible.\"</i> <p>" +
                    "You ask what you've done. <p>" +
                    "<i>\"Oh, nothing other than being the most infuriating man I've ever met. Repeatedly. Over and over again. After all, you can still talk,\"</i> She licks her lips, looks away from you, and begins rocking her hips, <i>\"Seems to be a pattern in these silly Games. But enough of that. What should I do with you?\"</i> <p>" +
                    "You say she could let you go. She only chuckles. <p>" +
                    "Your mind shifts away from making a case as her tail, which you forgot about as it hadn't moved around you dick since you came the first time, positions your member to the entrance of her pussy. Thing is, she isn't letting your dick go in. She's just holding it there, stroking it, letting you feel every curve of her tail. <p>" +
                    "<i>\"Beg.\"</i> <p>" +
                    "You won't do that. Or at least, that's what you thought until you try moving and realize you can't. It seems like she's actually not letting you move until you actually beg. Reyka turns around, loosening her tail's grip only to reposition herself so the two of you are chest to chest, your dick once again teasing her entrance. With your hands stuck in their previous position, she doesn't have to put much effort in standing up as your hand are now cupped around her back and ass. <p>" +
                    "<i>\"Beg,\"</i> Her playful tone conflicts with her hungry eyes. You still try to resist and she pouts. That familiar spicy scent of hers intensifies and you close your eyes to try and concentrate, <i>\"Beg and submit to me. Prove to me that you are inferior and I'll let you cum as much as you want.\"</i> <p>" +
                    "Closing your eyes was a mistake as her words reverb in your head. You can hear a voice telling you it's okay. That she won't do anything to you. That she wants this as much as you do. Despite those words you try to fight it, holding on as long as you can. <p>" +
                    "When it's almost too much you open your eyes. Reyka's pout has turned into a scowl. You notice the same hunger in her eyes but their wavering as sweat slowly forms on her brow. Even though you're on the edge, you can see she's putting up an actual effort in influencing you. That realization breaks your concentration and words escape your mouth before your brain can keep up, screaming for release. <p>" +
                    "The magic controlling your limbs break and you wrap yourself around Reyka, thrusting into her hole without a care for the world. Reyka stands still and lets out a held breath, content in letting you do all the work as she gently caresses and supports your body. <p>" +
                    "When you cum your world explodes in a flash of light. When your vision returns you find that Reyka caught you, her demonic strength more than enough to keep you afloat. <p>" +
                    "<i>\"Such a foolish man,\"</i> She whispers into your ear as you try to recover your breathe, <i>\"You humans submit to us demons all the time. Did you think you could resist forever?\"</i> <p>" +
                    "You attempt to murmur something but you're too drained. She laughs as she gently helps you steady yourself. <p>" +
                    "<i>\"You know, you've just submitted to a demon unconditionally. I could use that to make you my pet for real. Would you want that? I'll even make sure you'll become my exclusive toy.\"</i> <p>" +
                    "Your eyes widen at the offer. As tempting as it is, to be at Reyka's mercy for as long as you live, you shake your head. You can't just abandon everything to be hers. <p>" +
                    "<i>\"Hm,\"</i> She doesn't seem mad, just disappointed, <i>\"Yes, I guess it would take more to break you. Otherwise you wouldn't be worth making a pet.\"</i> <p>" +
                    "You blink. You ponder this as she makes to leaves. Before she turns around the corner she looks back and winks before going off to find another victim.<p>";
        }
		return "With a final cry of defeat (and pleasure) you erupt under Reyka's"
				+ " attentions. She immediately pounces on you and draws your lips to hers."
				+ " As she does so, she inhales deeply, drawing more than just air out of you."
				+ " You feel your strength flowing into her and even though your vision"
				+ " is already darkening, you see her starting to literally glow, surrounded"
				+ " by a soft, deep red aura.<p>"
				+ "As she continues to drink in your energy,"
				+ " you feel something shift in you, as if something that was there all"
				+ " along but has always gone unnoticed by you suddenly got yanked on."
				+ " Just before you pass out, you see her wings enveloping you both"
				+ " in a dark, warm cocoon.<p> "
				+ "After what seems like an eternity,"
				+ " but what actually lasted for only a few minutes, you wake up and"
				+ " drowsily look around. You can see Reyka sitting cross-legged a few feet"
				+ " away, her wings folded neatly behind her back and her eyes fixed on"
				+ " yours."
				+ (opponent.hasDick() ? " You notice a bottle of pearly"
						+ " white liquid behind her and can only guess where it came from."
						: "")
				+ " <p>"
				+ "<i>\"You tasted heavenly\"</i>, she says, while you are still too"
				+ " far gone to catch on to the irony of the statement, <i>\"So here I was,"
				+ " drinking in your delicious energy, and it struck me that if I claimed"
				+ " your soul now, I wouldn't be able to drink from you again. So I simply"
				+ " took a little nibble into it and let you recover. I will expect you to"
				+ " repay this kindness soon, and there is only one thing I will accept"
				+ " as payment. I'll leave you to figure out what it is.\"</i><p>"
				+ "With that,"
				+ " she walks off, her hips, barely covered by her short miniskirt,"
				+ " seductively waving good-bye. For now.";
	}

	@Override
	public String defeat(Combat c,Result flag) {
		character.arousal.empty();
		Character opponent=c.getOther(character);
		declareGrudge(opponent,c);
		if(flag == Result.anal && c.stance.sub(character)){
		    return "Your mind hazily focuses on the present as if waking from a dream. You become aware of the world around you one piece at a time. Reyka is in front of you. That's bad. She's turned away from you, bent over. That's good. Your body is worn out, likely from a match with her. That's bad. Reyka seems to be just as worn out, if not more so. That's good. You're currently engaged in mindlessly fucking her. That's bad. You're fucking her in the ass right now. That's probably less bad.<p>" +
                    "Somehow you must have gotten the idea that fucking Reyka the traditional way would be too dangerous and would play into her strengths. Fucking her ass instead would be safer and would better let you keep hold of your senses. Well, that was probably your thought process, not that it seems to have worked out that way in practice.<p>" +
                    "Back to the present, it seems that you've actually got the upper hand on Reyka here. Her body is tinged red, and she's letting out a series of pleasured moans. Her tail is even wrapped around your body, gently stroking your back to urge you on.<p>" +
                    "<i>\"Mmm... you can let yourself cum, you know,\"</i> Reyka says. She turns her head slightly to catch your eye, giving you a soft gaze. Her tail dances across your back playfully as she speaks. <i>\"There's no need to make me cum twice before you even cum once.\"</i><p>" +
                    "Twice? Has Reyka already orgasmed? You consider this for a moment, and then shake your head. She's just trying to trick you. She must have gotten into your head and somehow muddled your mind, and now she's trying to trick you into thinking you've already won in order to get you to give up. Well, that's not going to work. You shake your head at her and adjust your position. You're going to have to punish her for such a dirty trick, and your dick is already in the perfect place for it.<p>" +
                    "<i>\"Trick? No, but I... ahh!\"</i> Reyka's back arches as you pound your dick into her ass, her body shaking with pleasure. Her legs shudder as her orgasm overtakes her, and she nearly collapses to the floor.<p>" +
                    "Your lips curl up into a grin. Now it's time to take your prize. After holding yourself back for so long, you can finally let yourself succumb to the temptation. You thrust your hips wildly, pounding your cock in and out of Reyka's ass with ferocious abandon. Your mind can soon think about nothing other than fucking Reyka's brains out. And soon, it can't even think about that.<p>" +
                    "Your mind hazily focuses on the present as if waking from a dream. You become aware of the world around you one piece at a time. Reyka is in front of you. That's bad. She's turned away from you, bent over. That's good. Your body is worn out, likely from a match with her, and your hips are burning from overexertion. That's bad. Reyka seems to be just as worn out, if not more so. That's good. You're currently engaged in mindlessly fucking her. That's bad. You're fucking her in the ass right now. That's probably less bad.<p>" +
                    "Somehow you must have gotten the idea that fucking Reyka the traditional way would be too dangerous and would play into her strengths. Fucking her ass instead would be safer and would better let you keep hold of your senses. Well, that was probably your thought process, not that it seems to have worked out that way in practice. You're not even sure how long you've been doing this. With how sore your hips are right now, it could have been hours.<p>" +
                    "Back to the present, it seems that you've actually got the upper hand on Reyka here. Her body is flushed red, and she's letting out a series of ecstatic moans. Her tail is even wrapped around your body, squeezing your torso to urge you on.<p>" +
                    "<i>\"Haa... okay, that's enough...\"</i> Reyka says. She turns her head slightly to catch your eye, giving you a pleading gaze. Her tail weakly loosens its hold on your torso as she says, <i>\"That's twice now, and you still haven't cum once yet. Please just trust me on this.\"</i><p>" +
                    "Twice? Is Reyka seriously trying to trick you into thinking you've already made her cum twice? If she'd said once, you might have believed her, but twice is frankly ridiculous. Granted, with how sore your hips are now, it does feel like you've been fucking Reyka long enough to have made her cum twice, but can you really take that chance? No, it's best to play this safe and make sure you win now.<p>" +
                    "Reyka lets out a frustrated growl as you see through her ruse. She buries her head in her arms as she surrenders to the brutal ass-fucking she's about to receive. At least, you were planning to make it brutal, but all you can manage with your worn-out hips is rather tender lovemaking. Unfortunately, this means that with you not able to take control of the situation, Reyka is able to fight back, squeezing her ass in on your dick.<p>" +
                    "The succubus seems to have supernatural control over her sphincter, and it moves in a way you're pretty sure a human sphincter never could. Despite your best efforts to take control, your body just doesn't have enough energy left, and you're subjected to the pleasure of Reyka's ass until you can take no more. Finally, the pleasure is too much, and you let loose, filling Reyka's ass with your seed.<p>" +
                    "Somehow though, you manage to make Reyka cum at the same time, and she rides out her orgasm alongside you. She pants heavily as she collapses to the ground in front of you, just as worn out as you from the match.<p>" +
                    "You pull yourself back from Reyka, your dick sliding out of her ass, only for your legs to almost immediately give out on you. You're left on the floor as you try to regain your energy. It seems Reyka isn't doing much better as she rolls over onto her side, gazing weakly at you.<p>" +
                    "<i>\"I have to admit you earned that victory... twice over,\"</i> Reyka says. She places a hand on the ground and struggles to push herself up. She initially fails and collapses back to the ground, but she manages to get up on her second attempt. Once she finally makes it to her feet, she gives you a wicked grin. <i>\"I'll have to remember to pay you back for that next time. Don't be surprised if I conveniently forget next time I've won and keep at you until I've drained you dry.\"</i><p>" +
                    "You raise an eyebrow at Reyka. Wait, she wasn't trying to trick you? You shake your head, trying to shake loose some memories of what had happened, but nothing comes to you. You let out a sigh as you search around to find your clothes. It would have been nice to be able to remember that, but a win is a win.<p>";
        }
		if(flag == Result.intercourse){
			Global.gui().displayImage("premium/Reyka Wall.png", "Art by AimlessArt");
			return "These days, you often wonder if your epitaph is going to be <i>\"It seemed like a good idea at the time.\"</i><p>"
					+ "Like now, for instance.<p>"
					+ "Reyka left an opening, or maybe let you think she had, and you took it, or maybe she made you want to take it. "
					+ "You can't exactly remember what happened here. It's been at least thirty seconds and there's a lot going on "
					+ "in your life right now.<p>"
					+ "Whatever it was, you ended up fucking her. This was absolutely the soundest strategy available to you at the "
					+ "time. You can't think of why you'd have done anything else, even though you knew from the moment you slid "
					+ "inside her that it was a terrible idea.<p>"
					+ "Now she isn't letting you pull out.<p>"
					+ "You aren't fucking her anymore so much as trying to escape, but her legs are locked around your waist, she's "
					+ "using her wings for wind resistance, and the two of you stumble around the room together like you're both "
					+ "angry drunks.<p>"
					+ "You can't make sounds that aren't animal groans and she won't. Stop. Smiling. She knows she's won this one "
					+ "and you're just playing out the string. Her next meal is coming in fast, and maybe a little more than that. "
					+ "Her tail's wound around your leg, and every so often, the tip pokes your sphincter in an attempt to break "
					+ "your concentration. Something's got to give and you're sure it'll be you.<p>"
					+ "You manage to spin as the two of you lurch back across the room and into the nearest hallway. Reyka loses "
					+ "some air as her back hits the wall, and just for a second, you feel things change around the two of you. "
					+ "Her concentration just lapsed.<p>"
					+ "You capitalize on that and put your hands on her thighs for leverage, so you can fuck her as hard as you "
					+ "can with what little you've got left in you. It's one last desperate attempt, not even to make her come, "
					+ "but to at least get her to stop fucking smiling like that.<p>"
					+ "To your relief, she does. Reyka's eyes widen, her fingers sink into your shoulder blades, and you get a "
					+ "welcome burst of adrenaline. You give Reyka all of it, slamming your hips against hers hard enough that "
					+ "you half expect to put her through the wall.<p>"
					+ "It surprises both of you when she comes first. You feel a dull flash of pain as her fingers and thighs "
					+ "tighten on you, but it's all part of the same envelope of sensation and it doesn't slow you down. "
					+ "Reyka's mouth opens in a soundless shout, she shudders out a climax against you, and the feel of triumph "
					+ "at that is more of an orgasm than the one you have a moment later.<p>"
					+ "Your legs decide they've had enough a moment later, and you clumsily stagger into a sitting position. "
					+ "Your cock begins to soften and pulls out of Reyka, who disengages from you and gently floats away.<p>"
					+ "<i>\"You're sure you're human?\"</i> she asks. <i>\"Absolutely sure?\"</i><p>"
					+ "You look at her strangely.<p>"
					+ "<i>\"No weird birthmarks you had removed? Was there anything weird about your parents, or your "
					+ "grandparents? You don't have any incubus blood at all?\"</i><p>"
					+ "Reyka runs two fingers through the cum leaking down her thighs and licks them clean. She looks sweaty "
					+ "and bedraggled, but no more exhausted for it. You, conversely, feel and probably look like a wrung-out "
					+ "dishtowel.<p>"
					+ "She settles to the ground on bare feet, fishes what's left of her clothing out of the wreckage in "
					+ "your wake, and kicks it over to you.<p>"
					+ "<i>\"You should look into it,\"</i> Reyka says. <i>\"No mere human could do what you do.\"</i><p>"
					+ "That might have been a compliment. You're not sure.<p>"
					+ "<i>\"And next time,\"</i> she says, <i>\"I might have to take you a little more seriously.\"</i><p>"
					+ "With that, she floats out the nearest window, out into the night.";
		}else if(character.has(Trait.limitedpotential)){
		    return "You've fought Reyka a few times and you've come up victorious on many of your encounters but something was different this round. <p>" +
                    "Despite her best attempt at resisting, Reyka quickly succumbs to your sexual techniques. She lets out a moan and you feel her vaginal walls shudder in orgasm.<p>" +
                    "As soon as you feel her love juices flow down you suddenly hit with a wave of arousal that floors you. When you regain your bearings you notice your hands are held above your head by some kind of magic circle. Reyka is on top of you, her pussy already lined up to your dick. <p>" +
                    "<i>\"You are such an arrogant human, you know that?\"</i> Not giving a chance to respond she sinks down onto you. A gasp escapes your mouth but she quickly closes it with her own. <p>" +
                    "She bucks her hips relentlessly, squeezing out what she can as quickly as possible. Each time she lifts her hips, you feel like your whole body is being sucked in. When she slams down her hips you can feel a wave of lust explode in and out of your body. If she continues this pace she might end up breaking your hips and force you out of the Games. <p>" +
                    "<i>\"Thinking you could outsex a succubus? That I've been playing with you on an even field?\"</i> She asks, <i>\"I think it's time I show you what a succubus truly is!\"</i> <p>" +
                    "For a split second you think that she might actually go through with her threats about taking your soul only for her hip slamming to double in intensity. You cum quickly, Reyka stopping for just a second to milk as much as she can before continuing her semen draining routine. <p>" +
                    "<i>\"Making me,\"</i> she states as she furiously swallows your cock per work, <i>\"a daughter of hell cum like some weakling? I'll show you who's in charge.\"</i> <p>" +
                    "She's not going to stop. You know it, your hips experience it, and your cock is barely conscious. You need to break out of this now. <p>" +
                    "Using what strength you have you break out of the magic cuffs. You lean up and hold onto her, kissing her back and groping her breast in an attempt to make her cum quickly. She's caught off guard, clearly not expecting you to take the lead but nonetheless tries her best to keep the pace going. <p>" +
                    "Soon enough, you're both close to hitting a second orgasm. Both of you could feel the other reaching their peak and try desperately to get the other off first. Her arms feeling around your chest, your hands on her shapely ass. Your shaft pumping deeper and deeper, her tongue wrestling yours to submission. <p>" +
                    "She reaches her peak first with you shortly after. You both latch onto each other to let your orgasms run their course. <p>" +
                    "<i>\"Yes, a human. Just an arrogant human.\"</i> She whispers as she gently plays with your hair. <p>" +
                    "You ask what's wrong only to receive chuckling in return. <p>" +
                    "<i>\"Honestly all of you should check for some demon ancestry,\"</i> She jokes, <i>\"It's inconceivable that mere humans are able to make me have to work this hard for a decent meal.\"</i> <p>" +
                    "You try to ask what she means but she quickly disengages from you. Your cum leaks down her thighs but she ignores it. Her brow furrows as she looks back at you but she quickly shakes her head and quickly turns back. <p>" +
                    "<i>\"Are they getting stronger or have I...?\"</i> <p>" +
                    "You can't make out the rest of the sentence as she walks away. <p>";
        }
		return "As you bring Reyka ever closer to her climax, her prehensile"
				+ " tail suddenly pulls you to the ground and coils around your neck."
				+ " She squats down over your face and uses her tail to push your face"
				+ " up against her pussy.<p>"
				+ "The scent is so enticing that you simply must"
				+ " have a taste, and you start lapping at her feverishly. Somewhere along"
				+ " the way her tail let go of your neck, the compulsion it provided having"
				+ " been replaced by that of her aphrodisiac juices.<p>"
				+ "As she finally screams out"
				+ " in orgasm, the sound reverberating through your soul, the amount of"
				+ " fluids gushing into your willing mouth nearly drown you. After a few"
				+ " seconds she rolls off of you, although you don't notice it, having passed"
				+ " out from the overdose of aphrodisiacs.<p>"
				+ "You come to your senses just"
				+ " in time to see Reyka drinking down the load of cum you are shooting"
				+ " into her mouth. Somehow, you keep from passing out as she drinks"
				+ " some of your energy and soon, you see her face hovering over yours.<p>"
				+ " <i>\"Sorry about that, but you wore me out so thoroughly I just needed"
				+ " a little snack to get back on my feet. You'll be perfectly fine in"
				+ " a few minutes, after that we can go back to hunting each other. I"
				+ " certainly hope we meet again soon, that treatment you gave me is well"
				+ " worth the trouble of fighting through the others to get at you.\"</i>"
				+ " As she walks away into the distance you can't help but feel like"
				+ " 'winning' is not quite the word you would use to describe what you"
				+ " just went through.";
	}

	@Override
	public String victory3p(Combat c, Character target, Character assist) {
		character.clearGrudge(target);
		character.clearGrudge(assist);
		if (target.human()) {
			return "<i>\"How kind of you to hold him for me, dear.\"</i> Reyka bows her head ever so slightly towards "+assist.name()+" and then turns her gaze upon you prone form. " +
					"She pulls a blindfold out of a small pocket in her miniskirt and secures it tightly over your eyes. <i>\"Wouldn't want to spoil the surprise, would we?\"</i> For " +
					"just a moment, you feel a slight pull on you mind, but the sensation passes quickly, replaced by that of one of her slender fingers invading your mouth. " +
					"It is covered with a fragrant liquid and given what you already know about her, there is little doubt in your mind of its origins. Your suspicions " +
					"are proven correct when the aphrodisiac reaches your loins, which respond as expected.<p>"
					+ "Apparently not one to stand on ceremony, Reyka immediately " +
					"settles over your now rock hard dick and lowers herself down onto it. The sensation is beyond comparison, her pussy wiggles and twists around you almost " +
					"as if it has a mind of its own, a mind connected to your own, knowing what will bring you the most pleasure. Your experiences in sex-fighting have " +
					"left you with impressive sexual stamina, but in the face of a succubus' unimpeded attentions, no man can hope to last. All too quickly you succumb " +
					"to the feelings, pouring your seed into the succubus. Just your seed. You were expecting her to take so much more, but you just feel a little tired, " +
					"not more so than after a regular orgasm.<p>"
					+ "The mystery is unveiled when Reyka removes the blindfold with her left hand. In her right hand, she is " +
					"holding a bottle. That bottle is firmly planted against the head of your still twitching dick and filled with your cum. <i>\"You looked scrumptious, " +
					"sitting there all helpless, but I was really in need of some supplies. Still, I didn't want to deny you the pleasure, so I crafted a teeny tiny " +
					"illusion just for you.\"</i> As she says this, she pours a small drop of your semen onto her finger and licks it up. <i>\"Yum, I might just have to find " +
					"you again later.\"</i> Both she and "+assist.name()+" walk off, in opposite directions, the former holding your clothes and the latter quietly giggling at " +
					"your embarrassment. Ah, well.";
		}
		else if(target.hasDick()){
			return "At the sight of "+target.name()+"'s erect cock, Reyka wraps her soft hands around it, slowly jerking up and down. "
					+ "Not seeing the reaction she wants, the succubus starts to fondle her breasts, arousing her prey. "
					+ "<i>\"You like that?\"</i> she asks, exposing her breasts and teasing the tip of her dick with her fingertip. "
					+ "Hungering for semen, she licks "+target.name()+"'s glans with her long tongue, enticing her hole. <p>"
					+ "Enjoying the expression on her face, Reyka starts to suck down on her manliness, welcoming her into her "
					+ "mouth. Feeling her hips start to move, the succubus begins to deep throat, allowing "+target.name()+"'s dick to "
					+ "reach the deepest parts of her throat. Feeling her dick starting to twitch a bit, she stops for a "
					+ "brief moment to catch air and prolong her orgasm. With her dick slipping out of her saliva-"
					+ "dripping mouth, the succubus manages to mutter out a sentence while slowly fondling her balls, "
					+ "<i>\"I've tasted better, but you're not so bad either, let's do something that'll feel even better...\"</i><p>"
					+ "Seeing that her prey's starting to emit pre-cum, the succubus decides it's time to heat things up "
					+ "a bit. She stands up and removes all her clothing, spreading her pussy dominantly.. <i>\"You "
					+ "think you can handle this?\"</i> she says standing over her lustful prey's wet cock.<p>"
					+ "Reyka grips "+target.name()+"'s dick in a swift motion and holds it at the edge of her hole, "
					+ "<i>\"Let's see how long you'll last.\"</i> The succubus gives a faint smile while drilling her opponent's penis into her deepest cavities, pleasuring them both. "
					+ "<p>"
					+ "The succubus rides "+target.name()+"'s dick roughly against her insides, making sure to finish her off quickly. "
					+ "At the critical moment, she quickly stops to exit and swallow her load. Cum fills her "
					+ "mouth, dripping down her throat and chin. She swallows everything in a single gulp.<p>"
					+ "Smiling at you, the succubus says in a devilish manner, <i>\"Thanks for helping, but I'm not quite "
					+ "feeling satisfied yet, mind helping me out again?\"</i>";
		}
		return "<i>\"My my, what a cute little offering you have caught for me tonight\"</i>, Reyka says, looking you at you with a satisfied grin on her face. <i>\"Not very nutritious, " +
				"but certainly a good deal of fun.\"</i> With that, she starts gently undressing "+target.name()+". When she is finished she squats down in front of her, bringing " +
				"her tail up between them. <i>\"Where would you prefer it dear?\"</i>, she asks "+target.name()+", whose eyes grow wide in shock. She manages to stammer out a " +
				"few syllables, but nothing quite coherent. \"No preference? Then I guess I will simply choose for you\" She brings her spade-tipped tail between "+target.name()+"'s " +
				"legs and starts running the very tip rapidly across her labia. When it is sufficiently wet, she moves it slightly upwards and moves it briskly back and forth over " +
				target.name()+"'s clit.<p>"+target.name()+", at first scared, now has her eyes closed and begins moaning feverishly. Just when she has almost reached her climax, " +
				"Reyka digs her tail deep into "+target.name()+"'s drooling pussy. This sends "+target.name()+" loudly over the edge. Her screams of pleasure are almost deafening, " +
				"and you have to work really hard to restrain her convulsing body. After a minute or so, the orgasm subsides and "+target.name()+" falls asleep and you gently lay her " +
				"down. When you turn to look at "+target.name()+", you are startled by the predatory look in her eyes. <i>\"I'm afraid all the excitement has left me a tad peckish. Be a " +
				"dear and help me out with that, will you?\"</i> You ponder whether or not you made a mistake in helping her.";
	}

	@Override
	public String intervene3p(Combat c, Character target, Character assist) {
		if (target.human())
			return "Your fight with "
					+ assist.name()
					+ " starts out poorly; she already"
					+ " has you naked and aroused, whereas she seems as cool and calm as when"
					+ " you started. You haven't lost yet though, you just need to find an opening " +
					"and turn things around. A noise behind you causes you to turn and your vision is " +
					"filled with two piercing red eyes. <i>\"Kneel.\"</i> You drop to your knees involuntarily. " +
					"The rational part of your brain is telling you that Reyka is trying to dominate your " +
					"mind and you should resist, but what's the point? Reyka's tail binds your wrists and " +
					"she forces you to turn back to a bewildered "+assist.name()+". <i>\"I'm not poaching " +
					"your prey,\"</i> you hear her say. <i>\"He's all yours.\"</i>";

		return "Your fight with "
				+ target.name
				+ " starts out poorly; she already"
				+ " has you naked and aroused, whereas she seems as cool and calm as when"
				+ " you started. Fortune, though, seems to have a strange sense of humor as"
				+ " your salvation comes in the form of a winged demon swooping down on "
				+ target.name
				+ ". The two are briefly entangled in a ball of limbs and wings,"
				+ " but soon Reyka comes out on top. She is pinning "
				+ target.name
				+ " helplessly to the ground, holding her arms behind her back and"
				+ " locking her shoulders in place with her wings. The struggle has "
				+ "left " + target.name
				+ " completely naked and ready for you to"
				+ " take advantage of.";
	}
	
	public String watched(Combat c, Character target, Character viewer){
		return "Just being in the same room with Reyka is dizzying. If she's actively trying to get to you, a bout with her is like fighting through a fever dream.<p>"
					+ "It's easier to tell what she's doing when she's not the one you're focusing on. From this perspective, you can tell she's just toying with "+target.name()+". This is just about over.<p>"
					+ "Reyka feigns a moment of vulnerability, backpedals, and disappears around a blind corner. "+target.name()+" thinks she's on the verge of winning, shakes her head to clear it, and runs after her.<p>"
					+ "They've found a dark part of the building, where the lights don't quite reach, and the shadows erupt as "+target.name()+" passes them. Tendrils of wet, living darkness wrap around her wrists and ankles. She puts up a fight, wrenching her hands and legs free one at a time, but every tendril that breaks or loses its grip is replaced by two more.<p>"
					+ "You can barely see Reyka, half-hidden by shadows that are a little too deep to be entirely natural. She's got both hands up, directing the eruptions of darkness like a conductor with a baton.<p>"
					+ "A few minutes later, it's over. The shadows drag "+target.name()+" towards the wall, then against it and a couple of inches into the air, with her hands and feet obscured from view. Reyka's shoulders sag with what might be relief, and she walks towards "+target.name()+" with a little extra swagger in her hips.<p>"
					+ "She tears away the remnants of "+target.name()+"'s clothing, and leans in to rest her forehead against "+target.name()+"'s. Whatever she says, it makes "+target.name()+" wriggle furiously against her bonds, but it does no good. It's like her limbs are sunk to the joints in concrete.<p>"
					+ "Reyka laughs, delighted, and kisses her way down [Opponent's] body, slow and unhurried, just as if they aren't in a competition or at risk of being interrupted. She pauses to find and torment all of "+target.name()+"'s most sensitive areas, her collarbone, her nipples, the smooth plane of skin just below her navel. The tendrils move "+target.name()+"'s legs a bit further apart, and Reyka's mouth fastens onto "+target.name()+"'s slit.<p>"
					+ "She makes a sound like she wasn't expecting it to taste this good and dives in, making "+target.name()+" writhe and arch her back against the wall. It's not long before "+target.name()+" cries out, then sags against her bonds, coming down from the orgasm she couldn't keep herself from having.<p>"
					+ "Reyka looks up at her, licking "+target.name()+"'s taste off her lips and cheeks with a tongue that's just a little too long. "+target.name()+" says something, dejected, and Reyka answers it with a deep, messy kiss.<p>"
					+ "It looks like it might just be some kind of polite thank-you for the meal until you notice Reyka's tail rise up from behind her. It snakes between Reyka's legs, and "+target.name()+"'s eyes widen as its tip pokes at her pussy. She looks down, surprised, but not as surprised as someone else might be. It's the Games, after all, and it's not like Reyka hides her tail.<p>"
					+ "Reyka murmurs something. "+target.name()+" closes her eyes, then nods, just once. Reyka grins, showing more teeth than a human has, and sinks a few slow inches of her tail into "+target.name()+"'s pussy.<p>"
					+ "She rides its surface as she slides it in and out of "+target.name()+". Reyka pulls "+target.name()+" close against her with her arms around her waist, kissing her, as her tail pushes in a bit further, then stops. She asks a question, "+target.name()+" nods again, and Reyka begins to move the tail faster, out, then back in, half a foot or more of it disappearing into "+target.name()+" in each quick, sinuous thrust.<p>"
					+ "Reyka clings to "+target.name()+" for support as she brings herself off with her tail, grinding her clit against its surface as she fucks "+target.name()+" with it. Reyka pushes "+target.name()+"'s sweaty hair out of her face and says something; whatever it is makes "+target.name()+"'s eyes widen and face go blank for a second, despite the sensations, and Reyka laughs again. It trails off into a throaty, room-filling moan as she comes.<p>"
					+ "She waits a few minutes longer, until "+target.name()+"'s body shakes with one more orgasm, before Reyka pulls herself away. Reyka puts a final kiss against the tip of "+target.name()+"'s nose, then gathers her clothing from the floor and saunters away, directly into the shadows at the end of the hall.<p>"
					+ "You're almost positive there's no door there, but Reyka's gone anyway.<p>"
					+ "A few seconds later, the tendrils holding "+target.name()+" to the wall dissolve away into nothing. She falls to the floor, catching herself on her hands, breathing hard, before collecting herself and moving on.";
	}

	@Override
	public String describe() {
		return "Reyka the succubus stands before you, six feet tall with"
				+ " the most stunningly beautiful body you have ever seen."
				+ " Her long black hair enshrines her perfect face like a priceless"
				+ " painting, her breasts are large - yet not overly so -"
				+ " and impossibly firm. Her arms are slim and end in long-fingered,"
				+ " soft hands, nails polished shining red. Underneath, her long and"
				+ " perfectly formed legs and delicate feet stand in an imposing posture."
				+ " Behind her, you see a long, arrow tipped tail slowly waving around, as"
				+ " well as a pair of relatively small but powerful-looking bat wings.<br>"
				+ " Her gaze speaks of indescribable pleasure, but your mind reminds you"
				+ " of the cost of indulging in a succubus' body: Give her half a chance"
				+ " and she will suck out your very soul.";
	}

	@Override
	public String draw(Combat c,Result flag) {
		Character opponent=c.getOther(character);
		character.clearGrudge(opponent);
		if(flag == Result.intercourse) {
            return "As you and Reyka are both thrusting against each other for all you're worth, you feel your inevitable climax approaching very rapidly.<p>"
                    + "Just " +
                    "before you erupt into her, you notice a strange change come over her. Her tail and wings seem to evaporate before you and her white skin gains " +
                    "a slight tan. The challenging look in her eyes and confident little smile are replaced by an expression of surprise and shock. But you're barely aware " +
                    "of any of this. All you can see are her crimson irises giving way to a deep, radiant blue.<p>"
                    + "Just as you spot what you think might be a single tear " +
                    "forming in her right eye, both of you reach your orgasms. You were expecting to feel her power wash over you, to feel it pour into every nook " +
                    "and cranny of your soul and then draw that soul out and into her, but none of that happens. The only sensations are the pure, tantric bliss of " +
                    "simultaneous climax and a strange affection for the two deep blue eyes staring into yours.<p>"
                    + "When eventually the paradise you found yourself in " +
                    "once again gives way to the cold bitterness of reality and you see the first red spots already reappearing in Reyka's eyes, your " +
                    "rapidly souring mood is mollified by her passionate, warm embrace. She just holds you like that, not speaking, not moving, even as the demonic parts " +
                    "of her anatomy return to her shapely form.<p>"
                    + "After a few minutes, which might as well have been days for you, she lets go of you, leans " +
                    "back some, and softly speaks the two words you least expected ever to hear from her: <i>\"Thank you.\"</i><p>"
                    + "With that, she leaves what few clothes she " +
                    "usually wears, turns around and walks away. Not the domineering, seductively swaying stride you have grown used to, but rather a slower, more " +
                    "composed walk with her head slightly bowed. You are absolutely perplexed by this rare display of emotion, but after a while you gather your wits " +
                    "- and her clothes - and take off into the night. In the back of your mind, you know she will be back to hunt you down later, but this experience " +
                    "will remain entrenched in you memory for quite some time.";
        }else{
		    return "Reyka pulls away from you, panting, gasping, her chest heaving, her lips slightly pursed as she tries to catch her breath.  She raises a hand in a sign asking you to hold.  As tough as this fight has been, you give her that moment, likewise taking a moment to try to regain your own bearings.<p>" +
                    "<i>\"It seems,\"</i> she says, looking you up and down in a respectful way instead of her normal hungry lustful gaze, <i>\"That we're very evenly matched.  Too evenly matched.\"</i>  You can't help but nod in agreement.  <i>\"What would you say to a draw?\"</i> <p>" +
                    "Call it a draw? That's not how this works. The fight can't end until someone orgasms.<p>" +
                    "She purses her lips, and seems lost in thought for a moment.  Her gaze lingering on your rock hard cock as she considers what you just said.  Your gaze sweeping up and down her body, lingering on nipples which are so erect it looks painful, shining wet labia dripping with liquid lust, and the blush that covers her face and chest.  She's as close as you are, and you both know it.<p>" +
                    "<i>\"Ok, but if we cum at the same time...\"</i>  her words trail off as she saunters back towards you, but her touch is gentle, almost shy, her hand stroking down your arm to entwine your fingers in hers.  Her gaze upon yours as she whispers softly, <i>\"Trust me.\"</i>  But gone is the normal commanding haughty tone, instead one that is very human, and almost comes out sounding like a question.<p>" +
                    "You nod your head, and she pulls you down to the ground slowly.  You sink to your knees together, her gaze still upon yours, then she pushes you very gently back till you're laying flat on your back, your throbbing cock pointing upwards.  She gives a quick smile and you see a hint of that devilish nature and attitude of hers for just a moment, then she's swinging her knee over your head.  <p>" +
                    "Kneeling above you, you're greeted with the sight of her aroused and dripping wet pussy.  The smell is intoxicating, musky and just a bit spicy, giving truth to her demonic nature.  She lowers it and without asking you raise your head to meet it.  The taste filling your senses and becoming the only thing that matters.  But this was a draw and it seems she's not forcing you, for only a moment later you feel the lovely pillowy breasts pressing against your stomach.  Then a moment later the soft wet pleasure of her lips wrapping around the head of your engorged cock.  Her leathery bat wings close down around the both of you, encircling your bodies like a warm living blanket.<p>" +
                    "The sweet sixty nine between you two is unrushed and non combative, as you simply seem to be enjoying the taste and feel of each other's bodies.  Your tongue laps outwards, tracing circles around and between her labia before caressing the stiff button of her clit.  Likewise you feel her lips and tongue working your shaft in steady and unhurried fashion.  Her tongue sliding sinuously along your hard shaft as her lips pump up and down around the crown creating a delightful and much needed relief to the burning need you felt in your loins only moments before.<p>" +
                    "It continues for a time that feels both too short and at the same time just long enough.  You feel the unmistakable bliss that can't be denied racing through your body, and wrap your lips about her clit, flicking it with your tongue in rapid steady strokes.  Both of your bodies writhe against each other and it's unclear if one of you came a heartbeat before the other, or if you did manage to cum in each other's mouth at the same time.  But you release, letting your hot spunk flood into her waiting mouth as her pussy convulses and a fresh wave of her wet arousal covers your nose and lips.<p>" +
                    "For a few moments you simply hold each other's bodies as the golden wave of afterglow rolls softly through each of you.  Then with a sigh she sits up, letting your softening cock flop free from her lips.  Slinging her knee back over your head she kneels next to you.  You turn slightly on your side and look at her.  She smiles and licks her lips, though she didn't let a single drop of your spunk escape.  You smile at her as you swipe the back of your hand across your face, clearing away most of her lustful juices that had been left there.<p>" +
                    "<i>\"Like I said,\"</i> she smiles, her tone light and soft, almost a whisper, <i>\"evenly matched.\"</i>  You smile and nod, what else needs to be said.<p>" +
                    "Then a soft sigh escapes her lips and she stands up, you can almost see the change in her stance, her demeanour, as she looks down at you, fists on her hips, <i>\"But don't think I'm going to go easy on you next time.  Next fight you're going down!\"</i>  Your smile in response to that must make it clear how ironic that statement is.<p>" +
                    "She loses her confident vibe for just a moment and slightly stammers, <i>\"You know what I mean.  Ugh.\"</i>   Then with that she turns and walks off, her hips sashaying and swaying, as you lay there on the ground looking after her.  You could be wrong, but you can't help but think the way her tail curls and shifts behind her for just a moment, and only just a moment, forms into the perfect outline of a heart.<p>" +
                    "You lay back and smile, knowing that next time you're going to try to win as well, and even if you don't, well, that might be ok too";
        }
	}
	
	@Override
	public boolean fightFlight(Character paramCharacter) {
		return !this.character.nude() || Global.random(3) == 1;
	}

	@Override
	public boolean attack(Character paramCharacter) {
		return !this.character.nude() || Global.random(3) == 1;
	}

	@Override
	public void ding() {
		if(character.getLevel() >= 30){
			int rand = Global.random(3);
			if (rand == 0) {
				this.character.mod(Attribute.Power, 1);
			} else if (rand == 1) {
				this.character.mod(Attribute.Seduction, 1);
			} else if (rand == 2) {
				this.character.mod(Attribute.Cunning, 1);
			}
			character.getStamina().gain(2);
			character.getArousal().gain(3);
		}
		else{
			this.character.mod(Attribute.Dark, 1);
			for (int i = 0; i < (Global.random(3)/2)+1; i++) {
				int rand = Global.random(3);
				if (rand == 0) {
					this.character.mod(Attribute.Power, 1);
				} else if (rand == 1) {
					this.character.mod(Attribute.Seduction, 1);
				} else if (rand == 2) {
					this.character.mod(Attribute.Cunning, 1);
				}
			}
			character.getStamina().gain(4);
			character.getArousal().gain(6);
		}
	}

	@Override
	public String startBattle(Character opponent) {
		if(character.getGrudge()!=null){
			switch(character.getGrudge()){
				case enthralling:
					return "As you approach Reyka, her eyes suddenly flash bright red and you find yourself unable to look away. She slowly, deliberately "
							+ "approaches you and firmly grabs your crotch.<br>"
							+ "<i>\"Sometimes I think you underestimate me, just because you make me feel good. You're just a human, don't forget your place.\"</i> "
							+ "Against your will, you find yourself nodding to her. Her dark power and personality are too much for you to overcome "
							+ "with willpower alone.<p>"
							+ "She smiles, satisfied, and releases her grip on you. <i>\"If you understand, then we can begin.\"</i>";
				case succubusvagina:
					return "As you and Reyka face off, she grins wickedly at you and flashes her pussy. <i>\"Did you enjoy fucking me last time? I never thought a "
							+ "sexfighter would willingly stick his dick in a succubus, so I wasn't ready. Now that I know how much you want to cum inside me, "
							+ "I'll show you my favorite way to drain a man dry.\"</i>";
				case darkness:
					return "As you prepare to fight Reyka, you suddenly freeze in place. Something is different about her. She's radiating an intense, malicious "
							+ "aura. What the hell happened to her?<p>"
							+ "<i>\"To me? Nothing.\"</i> Reyka extends her wings in an intimidating manner. <i>\"Did the human forget I was a demon? Just because the "
							+ "cheeky human made me cum first, he forgot I'm the daughter of one of the most prestigious infernal families? Maybe that's what happened "
							+ "to me. What do you think is about to happen to that cheeky little human?\"</i><p>"
							+ "Oh shit, she's really holding a grudge over her last loss. She'll be going all out this time.";
				case strapped: //Added grudge, currently inacessible.
					return "Reyka seems to contemplate for a moment before fixing you with a wicked grin and strutting over. Suddenly, she reaches down and "
							+ "unfastens her miniskirt before throwing it aside dramatically. She strikes a provocative pose and "
							+ "a heat rises in your loins as you realize she's already wearing her strap-on! Did she have that on before...?<p> "
							+ "\"<i>You know, I've been getting curious. What kind of reaction will you give me if I shove this into your ass now? "
							+ "Will you moan for me, scream in pleasure? Beg me to stop? Maybe you'll beg me for more...</i>\"<p>"
							+ "You should probably try to get that cock off of her. Then again...";
			default:
				break;		
			}
		}
		if(character.nude()){
			return "Reyka coyly covers her naked body with her wings. <i>\"Don't you have any decency? Shouldn't you look away from an undressed lady?\"</i>"
					+ "She laughs and folds her wings behind her, exposing herself. <i>\"I guess you can't take your eyes off me. Fortunately, I'm not easily embarrassed.\"</i>";
		}
		if(opponent.pantsless()){
			return "Reyka's eyes focus on your exposed groin. <i>\"What a delicious looking cock you have. I'd like to taste that a couple "
					+ "of different ways. What do you say?\"</i>";
		}
		if(character.getAffection(opponent)>=30){
			return "Reyka gives you a smile that's more pleasant than predatory. <i>\"You're quickly becoming my favorite human. My prey isn't "
					+ "usually such good company.\"</i> She licks her lips, and you see a dangerous glint in her eyes. <i>\"Of course, that makes "
					+ "your seed even tastier. Don't blame me if I get a little carried away.\"</i>";
		}

		return "<i>\"Yum, I was just looking for a tasty little morsel.\"</i><p>"
				+ "Reyka strikes a seductive pose and the devilish smile"
				+ " on her face reveals just what, or more specifically,"
				+ " who she intends that morsel to be.";
	}

	@Override
	public boolean fit() {
		return (!this.character.nude() || Global.random(3) == 1)
				&& (this.character.getStamina().percent() >= 50)
				&& (this.character.getArousal().percent() <= 50);
	}

	@Override
	public String night() {
		return "You feel exhausted after yet another night of sexfighting. You're not complaining, of course; "+
				"what guy would when having this much sex with several different girls? Still, a weekend would "+
				"be nice sometime...<p>"
				+ "About half way to your room, Reyka steps in front of you. Where did she come from? "+
				"<i>\"Listen, "+Global.getPlayer().name()+", I've been doing some thinking lately. You know very well I've had sex with a lot "+
				"of guys and a fair amount of girls, too, right?\"</i><p>"
				+ "You just nod, wondering where this is going. <i>\"Well, "+
				"in all that time no one has ever made me feel the way you can. I don't know why, really, but I can't help "+
				"feeling there's something special about you.\"</i><p>"
				+ "You stand there, paralyzed, with a look of amazement "+
				"on your face. Reyka intimidates you. Hell, she is downright terrifying at times. To see and hear "+
				"her like this is like nothing you had ever expected from her. For a moment, you think this is all some "+
				"elaborate trick of some sort, but that thought vanishes the instant you see tears welling in her eyes.<p>"+
				"<i>\"I just... We demons aren't supposed to feel like this, you know? We don't form relationships. It's all "+
				"just a constant power struggle, constant scheming and looking over your shoulder and sleeping with a "+
				"knife under your pillow. It has never bothered me before; it's simply what I am. That's what I used to "+
				"think, anyway. Now, I'm not so sure... about anything...\"</i> She quitely sobs while saying this, and you "+
				"embrace her.<p>"
				+ "You hold her there for some time, before inviting her to spend the night at your place. "+
				"You don't even have sex when you get there, you just both lay down in your single bed, close to "+
				"each other, and enjoy a peaceful sleep together with your arms around her and her head on your shoulder.";
	}

	@Override
	public void advance(int rank) {
	    if(rank >= 3 && !character.has(Trait.limitedpotential)){
	        character.add(Trait.limitedpotential);
        }
		if(rank >= 3 && !character.has(Trait.infernalexertion)){
			character.add(Trait.infernalexertion);
		}
		if(rank >= 2 && !character.has(Trait.royalguard)){
			character.add(Trait.royalguard);
		}
		
	}
	@Override
	public NPC getCharacter() {
		return character;
	}
	
	public boolean checkMood(Emotion mood, int value) {
		switch(mood){
		case dominant:
			return value>=25;
		case nervous:
			return value>=80;
		default:
			return value>=50;
		}
	}
	@Override
	public float moodWeight(Emotion mood) {
		switch(mood){
		case dominant:
			return 1.2f;
		case nervous:
			return .7f;
		default:
			return 1f;
		}
	}

	@Override
	public String image() {
		return "assets/reyka_"+ character.mood.name()+".jpg";
	}
	@Override
	public void pickFeat() {
		ArrayList<Trait> available = new ArrayList<Trait>();
		for(Trait feat: Global.getFeats()){
			if(!character.has(feat)&&feat.req(character)){
				available.add(feat);
			}
		}
		if(available.isEmpty()){
			return;
		}
		character.add((Trait) available.toArray()[Global.random(available.size())]);
	}

	@Override
	public String resist3p(Combat c, Character target, Character assist) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public CommentGroup getComments() {
		CommentGroup comments = new CommentGroup();
		comments.put(CommentSituation.VAG_DOM_CATCH_WIN, "<i>\"Ah, yes! Give me more!\"</i>");
		comments.put(CommentSituation.VAG_DOM_CATCH_LOSE, "<i>\"I can't lose! Not like this!\"</i>");
		comments.put(CommentSituation.VAG_SUB_CATCH_WIN, "<i>\"Are you regretting fucking me now? It's time for you to cum now.\"</i>");
		comments.put(CommentSituation.VAG_SUB_CATCH_LOSE, "<i>\"Damn it! How did you get this good?!\"</i>");
		comments.put(CommentSituation.ANAL_PITCH_WIN, "<i>\"Feeling good? Do try to hold on pet, it would be a shame to waste your delicious semen...</i>\"");
		comments.put(CommentSituation.ANAL_PITCH_LOSE, "<i>\"Mmmnph, oh that's good. Hold that expression for me pet, I'm gonna...!\"</i>");
		comments.put(CommentSituation.ANAL_CATCH_WIN, "<i>\"Thought my ass would be harmless, did you?\"</i>");
		comments.put(CommentSituation.MOUNT_DOM_WIN, "<i>\"Mmmm. I wonder if you'll cum right away if I put it in. Shall we find out?\"</i>");
		comments.put(CommentSituation.MOUNT_SUB_WIN, "<i>\"Hah! Even on top it's hopeless! Now fuck me and finish it!\"</i>");
		comments.put(CommentSituation.MOUNT_SUB_LOSE, "<i>\"Please... At least feed me...\"</i>");
		comments.put(CommentSituation.OTHER_ENTHRALLED, "<i>\"You're a good little slave, aren't you?\"</i>");
		comments.put(CommentSituation.OTHER_HORNY, "<i>\"You really just can't resist me, can you?\"</i>");
		comments.put(CommentSituation.SELF_BUSTED, "Reyka covers her pussy and moans deeply, though it is impossible to tell if it is from pain or pleasure.");
		return comments;
	}

	@Override
	public int getCostumeSet() {
		return 1;
	}

	@Override
	public void declareGrudge(Character opponent, Combat c) {
		if(character.getGrudge()==Trait.darkness || character.getGrudge() == Trait.succubusvagina || character.getGrudge() == Trait.strapped){ //Enthralling is a Tier 2 Grudge.
			character.addGrudge(opponent, Trait.enthralling);
		}
		else if(c.eval(character)==Result.intercourse){ //SV is a Tier 1 Special Grudge, triggers on intercourse defeats.
			character.addGrudge(opponent, Trait.succubusvagina);
		}
		else{ //Darkness is the default Tier 1 grudge. Added Strapped to grudges.
			if(Global.random(1) == 1 && Global.checkFlag(Flag.PlayerButtslut)){ //Blocked for now, currently in development.
				character.addGrudge(opponent, Trait.strapped);
			}
			else
				character.addGrudge(opponent, Trait.darkness);
		}
	}
    @Override
    public void resetOutfit() {
        character.outfit[0].clear();
        character.outfit[1].clear();
        this.character.outfit[0].add(Clothing.lbustier);
        this.character.outfit[1].add(Clothing.lminiskirt);
    }
}
