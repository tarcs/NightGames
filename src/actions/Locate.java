package actions;

import items.Item;
import items.Trophy;

import java.io.PrintWriter;
import java.io.StringWriter;

import areas.Area;
import global.Flag;
import global.Global;
import gui.GUI;
import characters.Character;
import characters.NPC;
import characters.Trait;

public class Locate extends Action {
	private static final long serialVersionUID = 1L;

	private Character target;

	public Locate(Character target) {
		super("Locate "+target.name(),"Teleport yourself to "+target.name()+"'s location.");
		this.target = target;
	}

	@Override
	public boolean usable(Character self) {
		return self.has(Trait.locator)&&self.has(target.getUnderwear())&&target!=self;
	}

	@Override
	public Movement execute(Character self) {
		Area area = target.location();
		Global.gui().clearText();
		Global.gui().message("Focusing on the essence contained in the "
				+ target.getUnderwear().getName()
				+ ". In your mind, an image of the "
				+ area.name
				+ " appears. It falls apart as quickly as it came to be, but you know where "
				+target.name()+" currently is. Your hard-earned trophy is already burning up into a portal leading to your prey.");
		self.spendArousal(15);
		self.consume(target.getUnderwear(), 1);
		self.location().exit(self);
		area.enter(self);
		return Movement.locating;
	}

	@Override
	public Movement consider() {
		return Movement.locating;
	}
	public boolean freeAction(){
		return true;
	}
}
