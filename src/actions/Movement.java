package actions;

import java.awt.Color;

public enum Movement {
	quad(" head outside, toward the quad.",new Color(250,250,250)),
	kitchen(" move into the kitchen.",new Color(250,250,250)),
	dorm(" move to the first floor of the dorm.",new Color(250,250,250)),
	shower(" run into the showers.",new Color(250,250,250)),
	storage(" enter the storage room.",new Color(250,250,250)),
	dining(" head to the dining hall.",new Color(250,250,250)),
	laundry(" move to the laundry room.",new Color(250,250,250)),
	tunnel(" move into the tunnel.",new Color(250,250,250)),
	bridge(" move to the bridge.",new Color(250,250,250)),
	engineering(" head to the first floor of the engineering building.",new Color(250,250,250)),
	workshop(" enter a workshop.",new Color(250,250,250)),
	lab(" enter one of the chemistry labs.",new Color(250,250,250)),
	la(" move to the liberal arts building.",new Color(250,250,250)),
	library(" enter the library.",new Color(250,250,250)),
	pool(" move to the indoor pool.",new Color(250,250,250)),
	union(" head toward the student union.",new Color(250,250,250)),
	hide(" disappear into a hiding place.",new Color(200,200,200)),
	trap(" start rigging up something weird, probably a trap.",new Color(120,200,120)),
	bathe(" start bathing in the nude, not bothered by your presence.",new Color(170,200,250)),
	scavenge(" begin scrounging through some boxes in the corner.",new Color(220,220,150)),
	craft(" start mixing various liquids. Whatever it is doesn't look healthy.",new Color(220,220,150)),
	wait(" loitering nearby",new Color(200,200,200)),
	resupply(" heads for one of the safe rooms, probably to get a change of clothes.",new Color(170,200,250)),
	oil(" rubbing body oil on her every inch of her skin. Wow, you wouldn't mind watching that again.",new Color(100,250,250)),
	energydrink(" opening an energy drink and downing the whole thing.",new Color(100,250,250)),
	beer(" opening a beer and downing the whole thing.",new Color(100,250,250)),
	recharge(" plugging a battery pack into a nearby charging station.",new Color(170,200,250)),
	locating(" is holding someone's underwear in her hands and breathing deeply. Strange.",new Color(170,200,250)),
	masturbate(" starts to pleasure herself, while trying not to make much noise. It's quite a show.",new Color(250,170,170)),
	mana(" doing something with a large book. When she's finished, you can see a sort of aura coming from her.",new Color(170,200,250)),
	retire(" has left the match.",new Color(250,100,100)),
	potion(" drink an unidentifiable potion.",new Color(100,250,250));
	
	private String desc;
	private Color color;

	/**
	 * @return the Item name
	 */
	public String describe()
	{
		return desc;
	}
	public Color getColor(){
		return color;
	}

	private Movement( String desc, Color buttoncolor )
	{
		this.desc = desc;
		this.color = buttoncolor;
	}
}
